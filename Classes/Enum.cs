﻿public enum DocStatus {
    pending_approval = 1,
    active = 2,
    deleted = 3
}

public enum Status {
    none = 0,
    active = 1,
    inactive = 2,
    deleted = 3
}

public enum OnlineStatus {
    none = 0,
    online = 1,
    offline = 2
}

public enum Gender {
    male = 1,
    female = 2
}

public enum AccountType {
    freelancer = 1,
    employer = 2
}

public enum JobMode {
    part_time = 1,
    full_time = 2
}

public enum ResponseStatus {
    success = 1,
    failed = 2,
    error = 3,
}

public enum PaymentBasis {
    fixed_price = 1,
    hourly = 2
}

public enum MessageStatus {
    sent = 1,
    delivered = 2,
    read = 3
}

public enum JobCategory {
    it = 1,
    accounting_and_finance = 2,
    education = 3
}

public enum TimeUnit {
    hour = 1,
    day = 2
}

public enum FileType {
    freelancer_document = 1,
    user_prof_pic = 2,
    company_logo = 3,
    job_file = 4,
    project_file = 5,
    company_document = 6
}

public enum SqlTable {
    tbl_audit_trail,
    tbl_bookmark,
    tbl_employer,
    tbl_config,
    tbl_file,
    tbl_freelancer,
    tbl_job,
    tbl_job_application,
    tbl_lu_country,
    tbl_lu_gender,
    tbl_lu_status,
    tbl_lu_user_type,
    tbl_lu_yes_no,
    tbl_message,
    tbl_project,
    tbl_project_bid,
    tbl_review,
    tbl_user
}

public enum Configuration {
    none = 1,
}

public enum YesNo {
    yes = 1,
    no = 2,
    none = 3,
}

public enum NotificationType {
    success = 1,
    error = 2,
    warning = 3,
    info = 4
}

public enum NotificationSide {
    none,
    top,
    left,
    bottom,
    right
}

public enum RequestType {
    register_user = 1,
    login = 2,
    post_job = 3,
    post_project = 4,
    freelancer_edit = 5,
    file_upload = 6,
    employer_edit = 7
}