﻿using System;
using System.Web;

public class UserSession : UserClass {
    public static void setUser( string _sUserGuid, bool _bKeepSession = false ) {
        if ( _bKeepSession ) {
            HttpContext.Current.Session.Timeout = 20160 /*tot mins in 14 days*/;
        } else {
            HttpContext.Current.Session.Timeout = 30;
        }
        UserClass _UserClass = getObj( _sUserGuid );
        if ( _UserClass != null ) {
            HttpContext.Current.Session["user"] = new SessionUserMdl( _UserClass.Uuid, _UserClass.Email, _UserClass.Name + " " + _UserClass.Surname, _UserClass.Name, _UserClass.AccountType);
        }
    }

    public static void killUserSession() {
        HttpContext.Current.Session["user"] = null;
    }

    public static void redirect( string _sRedirectUrl ) {
        if ( string.IsNullOrEmpty(getUser().UserKey)) {
            HttpContext.Current.Response.Redirect( "/home/" );
        } else {
            HttpContext.Current.Response.Redirect( _sRedirectUrl );
        }
    }

    public static void checkLogIn() {
        if ( ( string.IsNullOrEmpty(getUser().UserKey)) ) {
            HttpContext.Current.Session["nextpage"] = HttpContext.Current.Request.Url.AbsoluteUri;
            HttpContext.Current.Response.Redirect( "/home/" );
        } else {
            HttpContext.Current.Session["nextpage"] = null;
        }
    }

    public static string checkLogInAjax() {
        if ( ( string.IsNullOrEmpty(getUser().UserKey)) ) {
            return "<p class='err_mess'>You are nolonger logged in. Your request has been aborted. Click <a href='login?r=true'>here</a> to log in.</p>";
        }
        return String.Empty;
    }

    public static void checkUserLogIn() {
        if ( ( string.IsNullOrEmpty(getUser().UserKey)) ) {
            HttpContext.Current.Response.Redirect( "/home/" );
        }
    }

    public static string getCurrentUserKey() {
        return getUser().UserKey;
    }

    public static string getUserFullName() {
        return getUser().Fullname;
    }

    public static string getUserEmail() {
        return getUser().Email;
    }

    public static string getUserFirstName() {
        return getUser().FirstName;
    }

    public static AccountType getAccountType() {
        return getUser().AccountType;
    }

    public static SessionUserMdl getUser() {
        try {
            if ( HttpContext.Current.Session["user"] != null ) {
                if ( HttpContext.Current.Session["user"] is SessionUserMdl ) {
                    return HttpContext.Current.Session["user"] as SessionUserMdl;
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "UserSession", "getCurrent", _Ex.Message );
        }
        return new SessionUserMdl();
    }
}