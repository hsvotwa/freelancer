﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class ListItem
{
    private Dictionary<string, dynamic> g_Properties = new Dictionary<string, dynamic>();

    public ListItem(string _sKey = "", dynamic _Value = null)
    {
        if (_sKey.Trim() != String.Empty
            && _Value != null)
        {
            this.setProperty(_sKey, _Value);
        }
    }

    public ListItem(string _sNameIndex, string _sName, string _sValueKey, dynamic _Value)
    {
        this.setProperty(_sNameIndex, _sName);
        this.setProperty(_sValueKey, _Value);
    }

    public dynamic getProperty(string _sKey)
    {
        try
        {
            if (this.g_Properties.ContainsKey(_sKey))
            {
                return this.g_Properties[_sKey];
            }
        }
        catch (Exception _Ex)
        {
            ErrorHandler.handleError("Class", "ListItem", "getProperty", _Ex.Message);
        }

        return "";
    }

    public bool inProperty(string _sKey)
    {
        return this.g_Properties.ContainsKey(_sKey);
    }

    public void setProperty(string _sKey, dynamic _sValue)
    {
        try
        {
            this.g_Properties[_sKey] = _sValue;
        }
        catch (Exception _Ex)
        {
            ErrorHandler.handleError("Class", "ListItem", "setProperty", _Ex.Message);
        }
    }
}