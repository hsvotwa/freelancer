﻿using System;
using System.Collections.Generic;
using System.Web.Helpers;

public class Mailer {
    public static bool send( string _sTo, string _sSubject, string _sBody, string _sCC = "", string _sBCC = "", IEnumerable<string> _Attachments = null ) {
        try {
            WebMail.SmtpServer = Constant.SMTP_SERVER;
            WebMail.EnableSsl = Constant.ENABLE_SSL;
            WebMail.UserName = Constant.WEBMAIL_USERNAME;
            WebMail.Password = Constant.WEBMAIL_PASSWORD;
            WebMail.From = Constant.FROM_EMAIL;
            WebMail.Send( _sTo, _sSubject, _sBody, cc: _sCC == "" ? null : _sCC, bcc: _sBCC == "" ? null : _sBCC, priority: "High", isBodyHtml: true, filesToAttach: _Attachments );
            return true;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Mailer", "send", _Ex.ToString() );
        }
        return false;
    }

    public static string getSignature() {
        string _sBody = String.Empty;
        try {
            _sBody += "<br/><img src=\"" + Constant.SIGNATUTE_IMAGE_URL + "\"/ height=\"80\" width=\"180\">";
            _sBody += "<br/>Phone: " + Constant.PHONE;
            _sBody += "<br/>Email: " + Constant.EMAIL;
            _sBody += "<br/><a href=\"" + Constant.DOMAIN_URL + "\" target=\"_blank\">" + Constant.DOMAIN_URL + "</a>";
        } catch ( Exception ) {
        }
        return _sBody;
    }
}