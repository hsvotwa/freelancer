﻿using System;
namespace Freelancer.Classes {
    public class RequestResponse {
        #region Member Variables
        protected ResponseStatus _ResponseStatus;
        protected string _sDescription;
        protected string _sJsFunc;
        #endregion

        #region Constructors
        public RequestResponse() {
        }

        public RequestResponse( ResponseStatus _ResponseStatus, string _sDescription, string _sJsFunc = "" ) {
            this._ResponseStatus = _ResponseStatus;
            this._sDescription = _sDescription;
            this._sJsFunc = _sJsFunc;
        }
        #endregion

        #region Public Properties
        public ResponseStatus ResponseStatus {
            get { return _ResponseStatus; }
            set { _ResponseStatus = value; }
        }
        public string Description {
            get { return _sDescription; }
            set { _sDescription = value; }
        }
        public string JsFunc {
            get { return _sJsFunc; }
            set { _sJsFunc = value; }
        }
        #endregion
    }
}
