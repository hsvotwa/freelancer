﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public class Input
{
    private string g_sName = ""; //The name is both the name and id of the element
    private InputType g_Type; //The type determines the input type, like text, radio, checkbox, and etc. Have a look at final class InputType for options
    private string g_sValue = "";
    private string g_sStyle = "";
    private string g_sCssClass = "";
    private string g_sCssClassPlaceholder = "placeholder"; //Set to what ever class you want when used, or before using the class.
    private string g_sOtherAttr = "";
    private int g_iNumDec = 2;
   // private string g_sDataSrc = ""; //This can either be an array or mysql data source
    private string g_sDefValue = "0"; //Default option value for a combobox (select)
    private string g_sDefOption = "-- please select --"; //Default option decription for a combobox (select)
    private bool g_bUseJquery = true;
    private bool g_bIsPost = true;
    private string g_sDataSrcValueIndex = "value";
    private string g_sDataSrcNameIndex = "name";
    private string g_sPlaceholder = ""; //Placeholder (HTML5)
    private bool g_bClearValue = false;
    private YesNo g_H5Rdy = YesNo.none;
    private string g_sSelectValue = "";
    private DataType g_DataType = DataType._string;
    private List<dynamic> g_lDataSrc = null;
    private string g_sJsDateFmt = "dd/mm/yy";
    private bool g_bInclDefOption = true;

    public string Name {
        get {
            return this.g_sName;
        }

        set {
            value = value.Trim();

            if ( value.Substring( 0, 1 ) == "#" ) {
                value = value.Substring( 1, value.Length ); //Remove # prefix.
            }

            this.g_sName = value;
        }

    }

    /// <summary>
    /// Selects type property. This will be the type of property to be created. This is done via Enumerator values.
    /// </summary>
    public InputType Type {
        set {
            this.g_Type = value;

            if ( this.g_Type == InputType.number
                    && this.g_DataType == DataType._string ) {
                this.g_DataType = DataType._double;
            }
        }
    }

    public DataType DataType {
        set {
            this.g_DataType = value;
        }
    }

    /// <summary>
    /// Create a value property. This will be the value of the field to be created.
    /// </summary>
    public string Value {
        get {
            if ( this.g_sValue == String.Empty ) {
                this.g_sValue = this.deterValue();
            }

            return this.g_sValue;
        }

        set {
            this.g_sValue = this.convValue( value );
        }
    }

    /// <summary>
    /// Creates the css property for the placeholder that is to be created.
    /// </summary>
    public string CssClassPlaceholder {
        get {
            if ( this.g_sCssClassPlaceholder == String.Empty
                && this.g_sCssClass != String.Empty ) {
                this.g_sCssClassPlaceholder = this.g_sCssClass + "_lightgrey";
            }

            return this.g_sCssClassPlaceholder;
        }

        set {
            this.g_sCssClassPlaceholder = value.Trim();
        }
    }

    public int NumDec {
        set {
            this.g_iNumDec = value;
        }
    }

    public string DefOption {
        set {
            this.g_sDefOption = value.Trim();
        }
    }

    public string DefValue {
        set {
            this.g_sDefValue = value.Trim();
        }
    }

    public string SelectValue {
        set {
            this.g_sSelectValue = value.Trim();
        }
    }

    public bool UseJquery {
        set {
            this.g_bUseJquery = value;
        }
    }
    /// <summary>
    /// Determaines if a post was made
    /// </summary>
    public bool IsPost {
        set {
            this.g_bIsPost = value;
        }
    }

    /// <summary>
    /// Gets and sets the index name of the datasource, i.e. the name to be used by a input field
    /// </summary>
    public string DataSrcNameIndex {
        get {
            return this.g_sDataSrcNameIndex;
        }

        set {
            this.g_sDataSrcNameIndex = value.Trim();
        }
    }

    /// <summary>
    /// Gets and sets the index value of the datasource, i.e. the value to be used by a input field
    /// </summary>
    public string DataSrcValueIndex {
        get {
            return this.g_sDataSrcValueIndex;
        }

        set {
            this.g_sDataSrcValueIndex = value.Trim();
        }
    }

    /// <summary>
    /// Gets and Sets the name of the placeholder of the input field that is created
    /// </summary>
    public string Placeholder {
        get {
            return this.g_sPlaceholder;
        }

        set {
            this.g_sPlaceholder = value.Trim();
        }
    }

    /// <summary>
    /// Determines whether the value of the field aftera post should be cleared or not.
    /// </summary>
    public bool ClearValue {
        set {
            this.g_bClearValue = value;
        }
    }

    public bool IsH5Rdy {
        get {
            if ( this.g_H5Rdy == YesNo.none ) {
                this.g_H5Rdy = (Validate.isH5Input( H5InputType.search ) ? YesNo.yes : YesNo.no);
            }

            return this.g_H5Rdy == YesNo.yes;
        }
    }

    public List<dynamic> DataSrc {
        set {
            this.g_lDataSrc = value;
        }
    }

    public string JsDateFormat {
        set {
            this.g_sJsDateFmt = value.Trim();
        }
    }

    public bool InclDefOption {
        get {
            return this.g_bInclDefOption;
        }

        set {
            this.g_bInclDefOption = value;
        }
    }

    public Input( string _sName = "", InputType _Type = InputType.none, string _sValue = "" )
    {
        if ( _sName.Trim() != String.Empty ) {
            this.Name = _sName;
        }

        if ( _Type != InputType.none ) {
            this.Type = _Type;
        }

        if ( _sValue.Trim() != String.Empty ) {
            this.Value = _sValue;
        }
    }

    public void setCssClass( string _sValue, bool _bAppend = true )
    {
        try {
            _sValue = _sValue.Trim();

            if ( _bAppend && this.g_sCssClass != String.Empty ) {
                this.g_sCssClass += " " + _sValue;
            } else {
                this.g_sCssClass = _sValue;
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "SQLUpdate", "setCssClass", _Ex.Message );
        }
    }

    public void setOtherAttr( string _sAttr, string _sValue = "", bool _bAppend = true, bool _bAppSemicol = false )
    {
        string _sCompAttr = _sAttr.Trim();

        try {
            if ( _sValue.Trim() != String.Empty ) {
                if ( !_bAppSemicol ) {
                    _sValue = _sValue.Trim().ToString();
                } else {
                    Conv.appSemicolon( ref _sValue );
                }

                _sCompAttr += "=\"" + _sValue + "\"";
            }

            if ( _bAppend
                    && this.g_sOtherAttr != String.Empty ) {
                this.g_sOtherAttr += " " + _sCompAttr;
            } else {
                this.g_sOtherAttr = _sCompAttr;
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "SQLUpdate", "setOtherAttr", _Ex.Message );
        }
    }

    public void setStyle( string _sValue, bool _bAppend = true )
    {
        try {
            Conv.appSemicolon( ref _sValue );

            if ( _bAppend && this.g_sStyle != String.Empty ) {
                this.g_sStyle += " " + _sValue;
            } else {
                this.g_sStyle = _sValue;
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "SQLUpdate", "setStyle", _Ex.Message );
        }
    }

    public string getHtml( bool _bGetInput = true, bool _bGetJs = true, int iMinYear = 10, int _iMaxYear = 10 ) {
        string _sJs = "", _sInput = "";

        try {
            if ( this.g_Type == InputType.select ) {
                _sInput = "<select ";
                _sInput += "name=\"" + this.g_sName + "\" id=\"" + this.g_sName + "\"";
                _sInput += " value=\"" + this.Value + "\"";

                if ( this.g_sCssClass != String.Empty ) {
                    _sInput += " class=\"" + this.g_sCssClass + "\"";
                }

                if ( this.g_sStyle != String.Empty ) {
                    _sInput += " style=\"" + this.g_sStyle + "\"";
                }

                if ( this.g_sOtherAttr != String.Empty ) {
                    _sInput += " " + this.g_sOtherAttr;
                }

                _sInput += ">";

                if ( this.InclDefOption ) {
                    _sInput += "<option value=\"" + this.g_sDefValue + "\">";
                    _sInput += this.g_sDefOption;
                    _sInput += "</option>";
                }

                if ( this.g_lDataSrc != null ) {
                    foreach ( dynamic _Row in this.g_lDataSrc ) {
                        if ( !( _Row is ListItem ) ) {
                            _sInput += "<option value=\"" + _Row[this.g_sDataSrcValueIndex] + "\"";

                            if ( _Row[this.g_sDataSrcValueIndex].ToString() == this.Value ) {
                                _sInput += " selected";
                            }

                            _sInput += ">";
                            _sInput += (string)_Row[this.g_sDataSrcNameIndex];
                            _sInput += "</option>";
                        } else {
                            _sInput += "<option value=\"" + (string)_Row.getProperty( this.g_sDataSrcValueIndex ) + "\">";
                            _sInput += (string)_Row.getProperty( this.g_sDataSrcNameIndex );
                            _sInput += "</option>";
                        }
                    }
                }

                _sInput += "</select>";
            } else if ( this.g_Type == InputType.textarea ) //Textarea
       {
                _sInput = "<textarea ";
                _sInput += "name=\"" + this.g_sName + "\" id=\"" + this.g_sName + "\""; //Name & ID

                if ( this.g_sCssClass != String.Empty ) {
                    _sInput += " class=\"" + this.g_sCssClass + "\""; //Css class
                }

                if ( this.g_sStyle != String.Empty ) {
                    _sInput += " style=\"" + this.g_sStyle + "\""; //Style
                }

                if ( this.g_sOtherAttr != String.Empty ) {
                    _sInput += " " + this.g_sOtherAttr; //Other attributes
                }

                if ( this.g_sPlaceholder != String.Empty ) {
                    if ( this.IsH5Rdy ) {
                        _sInput += " placeholder=\"" + this.g_sPlaceholder + "\""; //Placeholder (HTML5)
                    } else if ( this.g_bUseJquery ) {
                        _sJs += "<script>";
                        _sJs += "$(function() {";
                        _sJs += "$(\"#" + this.g_sName + "\").focus(function(){";
                        _sJs += this.addPlaceholderJs( true );
                        _sJs += "});";
                        _sJs += "$(\"#" + this.g_sName + "\").blur(function(){";
                        _sJs += this.addPlaceholderJs( false );
                        _sJs += "});";

                        if ( this.Value == this.g_sPlaceholder ) {
                            _sJs += "$(\"#" + this.g_sName + "\").removeClass(\"" + this.g_sCssClass + "\").addClass(\"" + this.CssClassPlaceholder + "\")";
                        }

                        _sJs += "});";
                        _sJs += "</script>";
                    }
                }

                _sInput += ">";
                _sInput += this.Value;
                _sInput += "</textarea>";
            } else {
                _sInput = "<input type=";

                switch ( this.g_Type ) {
                    case InputType.text:
                    case InputType.date:
                    case InputType.number:
                        _sInput += "\"text\"";

                        if ( this.g_bUseJquery ) {
                            if ( this.g_Type == InputType.date ) {
                                _sJs += "<script>";
                                _sJs += "$(function() {";
                                _sJs += "$(\"#" + this.g_sName + "\").datepicker({ dateFormat: \"" + this.g_sJsDateFmt + "\", changeMonth: true, changeYear: true, yearRange:\"-" + iMinYear.ToString() + ":+" + _iMaxYear.ToString() + "\", showButtonPanel: true  }); ";
                                _sJs += "});";
                                _sJs += "</script>";
                            } else if ( this.g_Type == InputType.number ) {
                                _sJs += "<script>";
                                _sJs += "$(function() {";
                                _sJs += "$(\"#" + this.g_sName + "\").blur(function(){";
                                _sJs += "$(\"#" + this.g_sName + "\").formatCurrency({";
                                _sJs += "symbol : \"\",";
                                _sJs += "negativeFormat : \"-%s%n\",";
                                _sJs += "roundToDecimalPlace : \"" + this.g_iNumDec.ToString() + "\"";
                                _sJs += "});";
                                _sJs += "});";
                                _sJs += "});";
                                _sJs += "</script>";
                            }
                        }

                        if ( this.g_sPlaceholder != String.Empty ) {
                            if ( this.IsH5Rdy ) {
                                _sInput += " placeholder=\"" + this.g_sPlaceholder + "\""; //Placeholder (HTML5)
                            } else if ( this.g_bUseJquery ) {
                                _sJs += "<script>";
                                _sJs += "$(function() {";
                                _sJs += "$(\"#" + this.g_sName + "\").focus(function(){";
                                _sJs += this.addPlaceholderJs( true );
                                _sJs += "});";
                                _sJs += "$(\"#" + this.g_sName + "\").blur(function(){";
                                _sJs += this.addPlaceholderJs( false );
                                _sJs += "});";

                                if ( this.Value == this.g_sPlaceholder ) {
                                    _sJs += "$(\"#" + this.g_sName + "\").removeClass(\"" + this.g_sCssClass + "\").addClass(\"" + this.CssClassPlaceholder + "\")";
                                }

                                _sJs += "});";
                                _sJs += "</script>";
                            }
                        }
                        break;
                    case InputType.password:
                    case InputType.pwd:
                        if ( this.g_sPlaceholder != String.Empty ) {
                            if ( this.IsH5Rdy ) {
                                _sInput += "\"password\"";
                                _sInput += " placeholder=\"" + this.g_sPlaceholder + "\""; //Placeholder (HTML5)
                            } else if ( this.g_bUseJquery ) {
                                _sInput += "\"text\"";
                                _sJs += "<script>";
                                _sJs += "$(function() {";
                                _sJs += "$(\"#" + this.g_sName + "\").focus(function(){";
                                _sJs += this.addPlaceholderJs( true, true );
                                _sJs += "});";
                                _sJs += "$(\"#" + this.g_sName + "\").blur(function(){";
                                _sJs += this.addPlaceholderJs( false, true );
                                _sJs += "});";

                                if ( this.Value == this.g_sPlaceholder ) {
                                    _sJs += "$(\"#" + this.g_sName + "\").removeClass(\"" + this.g_sCssClass + "\").addClass(\"" + this.CssClassPlaceholder + "\")";
                                }

                                _sJs += "});";
                                _sJs += "</script>";
                            }
                        } else {
                            _sInput += "\"password\"";
                        }
                        break;
                    case InputType.submit:
                        _sInput += "\"submit\"";
                        break;
                    case InputType.hidden:
                        _sInput += "\"hidden\"";
                        break;
                    case InputType.button:
                        _sInput += "\"button\"";
                        break;
                    case InputType.radio:
                        _sInput += "\"radio\"";
                        break;
                    case InputType.checkbox:
                        _sInput += "\"checkbox\"";

                        if ( this.g_bIsPost
                               && HttpContext.Current.Request.Form[this.g_sName] != null ) {
                            _sInput += " checked=\"checked\"";
                        } else if ( !this.g_bIsPost
                              && HttpContext.Current.Request.QueryString[this.g_sName] != null ) {
                            _sInput += " checked=\"checked\"";
                        }
                        break;
                }

                _sInput += " name=\"" + this.g_sName + "\" id=\"" + this.g_sName + "\""; //Name & ID
                _sInput += " value=\"" + this.Value + "\"";  //Value  

                if ( this.g_sCssClass != String.Empty ) {
                    _sInput += " class=\"" + this.g_sCssClass + "\""; //Css class
                }

                if ( this.g_sStyle != String.Empty ) {
                    _sInput += " style=\"" + this.g_sStyle + "\""; //Style
                }

                if ( this.g_sOtherAttr != String.Empty ) {
                    _sInput += " " + this.g_sOtherAttr; //Other attributes
                }

                _sInput += " />";
            }

            if ( _bGetInput
                    && _bGetJs ) {
                return _sInput + " " + _sJs.Trim();
            } else if ( _bGetInput ) {
                return _sInput;
            } else {
                return _sJs;
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "SQLUpdaInputte", "getHtml", _Ex.Message );
        }

        return "";
    }

    //Private, as this function doesn't work yet as desired
    private void add()
{
    try {
        HttpContext.Current.Response.Write( this.getHtml() );
    } catch ( Exception _Ex ) {
        ErrorHandler.logError( "Class", "Input", "add", _Ex.Message );
    }
}

private string deterValue()
{
    string _sValue = "";

    try {
        if ( this.g_bClearValue ) {
            if ( this.g_Type != InputType.select
                && this.g_sPlaceholder != String.Empty
                && !this.IsH5Rdy ) {
                return this.g_sPlaceholder;
            }

            return _sValue;
        }

        if ( this.g_bIsPost
            && HttpContext.Current.Request.Form[this.g_sName] != null ) {
            _sValue = HttpContext.Current.Request.Form[this.g_sName].ToString();
        } else if ( !this.g_bIsPost
              && HttpContext.Current.Request.QueryString[this.g_sName] != null ) {
            _sValue = HttpContext.Current.Request.QueryString[this.g_sName].ToString();
        }

        if ( _sValue == String.Empty ) {
            if ( this.g_Type == InputType.select ) {
                return this.g_sDefValue;
            } else if ( this.g_sPlaceholder != String.Empty
                  && !this.IsH5Rdy ) {
                return this.g_sPlaceholder;
            }

            return _sValue;
        }

        return this.convValue( _sValue );
    } catch ( Exception _Ex ) {
        ErrorHandler.logError( "Class", "SQLUpdate", "deterValue", _Ex.Message );
    }

    return _sValue;
}

private string convValue( string _sValue )
{
    try {
        switch ( this.g_DataType ) {
            case DataType.date:
                return Conv.toDate( _sValue, false, true );
            case DataType.date_time:
                return Conv.toDate( _sValue );
            case DataType._double:
            case DataType._float:
                return Conv.toNum( _sValue, false, this.g_iNumDec, "0.00" );
            case DataType.integer:
                return Conv.toNum( _sValue, false, 0 );
            default:
                return _sValue;
        }
    } catch ( Exception _Ex ) {
        ErrorHandler.logError( "Class", "Input", "convValue", _Ex.Message );
    }

    return _sValue;
}

private string addPlaceholderJs( bool _bFocus = true, bool _bChgType = false )
{
    string _sJs = "";

    try {
        if ( _bFocus ) {
            _sJs = "\"{placeholder}\"===$(\"{id}\").val()&&($(\"{id}\").val(\"\"),$(\"{id}\").removeClass(\"{css_class_placeholder}\").addClass(\"{css_class}\")";

            if ( _bChgType ) {
                //_sJs += ",$(\"{id}\").prop(\"type\",\"password\")";
                _sJs += ",$(\"{id}\").attr(\"type\",\"password\")";
            }
        } else {
            _sJs = "\"\"===$(\"{id}\").val()&&($(\"{id}\").val(\"{placeholder}\"),$(\"{id}\").removeClass(\"{css_class}\").addClass(\"{css_class_placeholder}\")";

            if ( _bChgType ) {
                //_sJs += ",$(\"{id}\").prop(\"type\",\"text\")";
                _sJs += ",$(\"{id}\").attr(\"type\",\"text\")";
            }
        }

        _sJs += ");";

        string[] _aSearch = new string[] { "{placeholder}", "{id}", "{css_class}", "{css_class_placeholder}" };
        string[] _aReplace = new string[] { this.g_sPlaceholder, "#" + this.g_sName + "", this.g_sCssClass, this.CssClassPlaceholder };
        int _iLen = _aSearch.Length;

        for ( int _iIndex = 0; _iIndex < _iLen; _iIndex++ ) {
            _sJs = _sJs.Replace( _aSearch[_iIndex], _aReplace[_iIndex] );
        }
    } catch ( Exception _Ex ) {
        ErrorHandler.logError( "Class", "Input", "addPlaceholderJs", _Ex.Message );
    }

    return _sJs;
}
}

public enum InputType
{
    none = 0,
    text = 1,
    password = 2,
    pwd = 3,  //Abbr. for password
    submit = 4,
    hidden = 5,
    button = 6,
    checkbox = 7,
    date = 8,
    number = 9,
    radio = 10,
    select = 11,
    textarea = 12
}

public enum DataType
{
    date = 1,
    date_time = 2,
    integer = 3,
    _string = 4,
    _double = 5,
    _float = 6
}