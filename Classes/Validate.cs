﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Text.RegularExpressions;

public class Validate
{
    //public static bool isBrowser(List<Browser> _sTypes, Browser _Browser = Browser.none) {
    //    try {
    //        string _sUserAgent = HttpContext.Current.Request.UserAgent.ToUpper();

    //        if (_Browser != Browser.none) {
    //            _sTypes = _sTypes.ToList();
    //        }

    //        foreach (var _aTypes in _sTypes) {
    //            switch (_aTypes) {
    //                case Browser.firefox:
    //                case Browser.ff:
    //                    if ((_sUserAgent.IndexOf("FIREFOX") != -1)) {
    //                        return true;
    //                    }
    //                    break;
    //                case Browser.internet_explorer:
    //                case Browser.ie:
    //                    if ((_sUserAgent.IndexOf("MSIE") != -1) && !(_sUserAgent.Contains("OPERA"))) {
    //                        return true;
    //                    }
    //                    break;
    //                case Browser.internet_explorer_7:
    //                case Browser.ie_7:
    //                    if ((_sUserAgent.IndexOf("MSIE 7.0") != -1) && !(_sUserAgent.Contains("OPERA"))) {
    //                        return true;
    //                    }
    //                    break;
    //                case Browser.internet_explorer_8:
    //                case Browser.ie_8:
    //                    if ((_sUserAgent.IndexOf("MSIE 8.0") != -1) && !(_sUserAgent.Contains("OPERA"))) {
    //                        return true;
    //                    }
    //                    break;
    //                case Browser.internet_explorer_9:
    //                case Browser.ie_9:
    //                    if ((_sUserAgent.IndexOf("MSIE 9.0") != -1) && !(_sUserAgent.Contains("OPERA"))) {
    //                        return true;
    //                    }
    //                    break;
    //                case Browser.chrome:
    //                    if ((_sUserAgent.IndexOf("CHROME") != -1)) {
    //                        return true;
    //                    }
    //                    break;
    //                case Browser.safari:
    //                    if ((_sUserAgent.IndexOf("SAFARI") != -1)) {
    //                        return true;
    //                    }
    //                    break;
    //                case Browser.opera:
    //                    if ((_sUserAgent.IndexOf("OPERA") != -1)) {
    //                        return true;
    //                    }
    //                    break;
    //            }
    //        }
    //    } catch (Exception _Ex) {
    //        ErrorHandler.handleError("Class", "Validate", "isBrowser", _Ex.Message);
    //    }
    //    return false;
    //}

    public static bool isH5Input( H5InputType _sTypes )
    {
        //try {
        //    List<Browser> _lBrowsers = new List<Browser>();

        //    switch (_sTypes) {
        //        case H5InputType.time:
        //            _lBrowsers.Add(Browser.safari);
        //            _lBrowsers.Add(Browser.chrome);
        //            _lBrowsers.Add(Browser.opera);
        //            return isBrowser(_lBrowsers);
        //        case H5InputType.date:
        //            _lBrowsers.Add(Browser.safari);
        //            _lBrowsers.Add(Browser.chrome);
        //            _lBrowsers.Add(Browser.opera);
        //            return isBrowser(_lBrowsers);
        //        case H5InputType.datetime:
        //            _lBrowsers.Add(Browser.safari);
        //            _lBrowsers.Add(Browser.chrome);
        //            _lBrowsers.Add(Browser.opera);
        //            return isBrowser(_lBrowsers);
        //        case H5InputType.datetime_local:
        //            _lBrowsers.Add(Browser.safari);
        //            _lBrowsers.Add(Browser.chrome);
        //            _lBrowsers.Add(Browser.opera);
        //            return isBrowser(_lBrowsers);
        //        case H5InputType.email:
        //            _lBrowsers.Add(Browser.ff);
        //            _lBrowsers.Add(Browser.chrome);
        //            _lBrowsers.Add(Browser.opera);
        //            return isBrowser(_lBrowsers);
        //        case H5InputType.month:
        //            _lBrowsers.Add(Browser.safari);
        //            _lBrowsers.Add(Browser.chrome);
        //            _lBrowsers.Add(Browser.opera);
        //            return isBrowser(_lBrowsers);
        //        case H5InputType.number:
        //            _lBrowsers.Add(Browser.safari);
        //            _lBrowsers.Add(Browser.chrome);
        //            _lBrowsers.Add(Browser.opera);
        //            return isBrowser(_lBrowsers);
        //        case H5InputType.range:
        //            _lBrowsers.Add(Browser.safari);
        //            _lBrowsers.Add(Browser.chrome);
        //            _lBrowsers.Add(Browser.opera);
        //            return isBrowser(_lBrowsers);
        //        case H5InputType.search:
        //            _lBrowsers.Add(Browser.safari);
        //            _lBrowsers.Add(Browser.chrome);
        //            return isBrowser(_lBrowsers);
        //        case H5InputType.tel: //Not yet supported by any browsers
        //            return false;
        //        case H5InputType.url:
        //            _lBrowsers.Add(Browser.ff);
        //            _lBrowsers.Add(Browser.chrome);
        //            return isBrowser(_lBrowsers);
        //        case H5InputType.week:
        //            _lBrowsers.Add(Browser.safari);
        //            _lBrowsers.Add(Browser.chrome);
        //            _lBrowsers.Add(Browser.opera);
        //            return isBrowser(_lBrowsers);
        //    }
        //} catch (Exception _Ex) {
        //    ErrorHandler.handleError("Class", "Validate", "isH5Input", _Ex.Message);
        //}

        return false;
    }

    //public static List<string> validateCompFields(List<string> _lFields) {
    //    List<string> _lReturn = new List<string>();
    //    string _sValue = "";

    //    try {
    //        foreach (string _sField in _lFields) {
    //            _sValue = HttpContext.Current.Request.Form[_sField].Trim();

    //            if (_sValue == String.Empty
    //                    || _sValue == "0") {
    //                _lReturn.Add(_sField);
    //            }
    //        }
    //    } catch (Exception _ex) {
    //        ErrorHandler.handleError("Class", "Validate", "validateCompFields", _Ex.Message);
    //    }

    //    return _lReturn;
    //}

    public static bool isEmail( string _sValue )
    {
        try {
            _sValue = _sValue.Trim();
            if ( _sValue == String.Empty ) { return false; }
            _sValue = Regex.Replace( _sValue, @"(@)(.+)$", getDomainMapper );  //Use IdnMapping class to convert Unicode domain names.
            return Regex.IsMatch(
                    _sValue,
                    @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z]{2,17}))$",
                    RegexOptions.IgnoreCase
                );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isEmail", _Ex.Message );
        }
        return false;
    }

    public static bool isNamibianPhone( string _sValue )
    {
        try {
            _sValue = _sValue.Trim();

            int _iOut = 0;

            if ( _sValue.Length != 10 || !int.TryParse( _sValue.Substring( 1 ), out _iOut ) || (!_sValue.StartsWith( "081" ) && !_sValue.StartsWith( "085" )) ) {
                return false;
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isNamibianPhone", _Ex.Message );
        }
        return true;
    }

    private static string getDomainMapper( Match _mMatch )
    {
        IdnMapping _Idn = new IdnMapping();  //IdnMapping class with default property values.

        try {
            string _sDomainName = _mMatch.Groups[2].Value;
            _sDomainName = _Idn.GetAscii( _sDomainName );
            return _mMatch.Groups[1].Value + _sDomainName;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "getDomainMapper", _Ex.Message );
        }
        return "";
    }

    public static bool isGuid( string _sValue )
    {
        try {
            _sValue = _sValue.Trim();
            if ( _sValue.Length != 36 ) { return false; }
            //Regex _RegexGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);
            Regex _Regex = new Regex( @"^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$", RegexOptions.Compiled );
            return _Regex.IsMatch( _sValue );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isGuid", _Ex.Message );
        }
        return false;
    }

    public static bool isDate( string _sValue, bool _bSqlFmt = false )
    {
        try {
            _sValue = _sValue.Trim();
            if ( _sValue.Length != 10 ) { return false; }
            Regex _Regex = new Regex(
                    (
                        !_bSqlFmt
                        ? @"^[0-9]{2}/[0-9]{2}/[0-9]{4}$"
                        : @"^[0-9]{4}-[0-9]{2}-[0-9]{2}$"
                    ),
                    RegexOptions.Compiled
                );
            return _Regex.IsMatch( _sValue );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isDate", _Ex.Message );
        }

        return false;
    }

    public static bool hasAccepDateSep( string _sValue, out string _sSeparator )
    {
        _sSeparator = string.Empty;

        try {
            string[] _sSeparators = new string[] { "/", "-", "." };

            foreach ( string _sSep in _sSeparators ) {
                if ( _sValue.Contains( _sSep ) ) {
                    _sSeparator = _sSep;
                    break;
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "hasAccepDateSep", _Ex.Message );
        }

        return _sSeparator.Trim() != String.Empty;
    }

    public static bool isFmtNotationYear( string _sValue )
    {
        try {
            if ( _sValue != _sValue.ToLower() )
                _sValue = _sValue.ToLower();
            string[] _sNotations = new string[] { "yy", "yyyy" };

            foreach ( string _sNotation in _sNotations ) {
                if ( _sNotation == _sValue )
                    return true;
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isFmtNotationYear", _Ex.Message );
        }

        return false;
    }

    public static bool isFmtNotationYear( string[] _sValues, out int _iIndex )
    {
        _iIndex = -1;

        try {
            for ( int _iI = 0; _iI < _sValues.Length; _iI++ ) {
                if ( isFmtNotationYear( _sValues[_iI] ) ) {
                    _iIndex = _iI;
                    return true;
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isFmtNotationYear", _Ex.Message );
        }

        return false;
    }

    public static bool isFmtNotationMonth( string _sValue )
    {
        try {
            if ( _sValue != _sValue.ToLower() )
                _sValue = _sValue.ToLower();
            string[] _sNotations = new string[] { "m", "mm" };

            foreach ( string _sNotation in _sNotations ) {
                if ( _sValue == _sNotation )
                    return true;
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isFmtNotationMonth", _Ex.Message );
        }

        return false;
    }

    public static bool isFmtNotationMonth( string[] _sValues, out int _iIndex )
    {
        _iIndex = -1;

        try {
            for ( int _iI = 0; _iI < _sValues.Length; _iI++ ) {
                if ( isFmtNotationMonth( _sValues[_iI] ) ) {
                    _iIndex = _iI;
                    return true;
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isFmtNotationMonth", _Ex.Message );
        }

        return false;
    }

    public static bool isFmtNotationDay( string _sValue )
    {
        try {
            if ( _sValue != _sValue.ToLower() )
                _sValue = _sValue.ToLower();
            string[] _sNotations = new string[] { "d", "dd" };

            foreach ( string _sNotation in _sNotations ) {
                if ( _sValue == _sNotation )
                    return true;
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isFmtNotationDay", _Ex.Message );
        }

        return false;
    }

    public static bool isFmtNotationDay( string[] _sValues, out int _iIndex )
    {
        _iIndex = -1;

        try {
            for ( int _iI = 0; _iI < _sValues.Length; _iI++ ) {
                if ( isFmtNotationDay( _sValues[_iI] ) ) {
                    _iIndex = _iI;
                    return true;
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Validate", "isFmtNotationDay", _Ex.Message );
        }
        return false;
    }

    public static bool isSchedule( string _sValue )
    {
        try {
            if ( !(_sValue is String) ) {
                return false;
            }
            if ( _sValue.Trim() == String.Empty ) {
                return false;
            }
            string[] _sParts = Conv.rmDblSpace( _sValue ).Split( ' ' );
            if ( _sParts.Length != 4 ) {
                return false;
            }
            if ( !isScheduleExp( _sParts[0], 0, 59 ) ) {
                return false;
            } //seconds
            if ( !isScheduleExp( _sParts[1], 0, 59 ) ) {
                return false;
            } //minutes
            if ( !isScheduleExp( _sParts[2], 0, 23 ) ) {
                return false;
            } //hours
            if ( !isScheduleExp( _sParts[3], 0, 6 ) ) {
                return false;
            } //day of week
            return true;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Valid", "isSchedule", _Ex.Message );
        }
        return false;
    }

    private static bool isScheduleExp( string _sExp, int _iMin, int _iMax )
    {
        int _iValue;

        try {
            if ( _sExp.Trim() == String.Empty ) {
                return false;
            }
            List<string> _lValues = new List<string>( _sExp.Split( ',' ) );

            if ( _lValues.Count == 1 ) {
                if ( int.TryParse( _lValues[0], out _iValue ) ) {
                    if ( !isBetween( _iValue, _iMin, _iMax ) ) {
                        return false;
                    }
                } else if ( _lValues[0] != "*" ) {
                    return false;
                }
                return true;
            }
            if ( _lValues.Distinct().Count() != _lValues.Count ) { //ensure unique set of expressions
                return false;
            }
            foreach ( string _sValue in _lValues ) {
                if ( !int.TryParse( _sValue, out _iValue ) ) {
                    return false;
                }
                if ( !isBetween( _iValue, _iMin, _iMax ) ) {
                    return false;
                }
            }
            return true;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Valid", "isScheduleExp", _Ex.Message );
        }
        return false;
    }

    public static bool isBetween( int _iValue, int _iMin, int _iMax, bool _bIncl = true )
    {
        try {
            return (
                _bIncl
                ? _iValue >= _iMin && _iValue <= _iMax
                : _iValue > _iMin && _iValue < _iMax
            );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Valid", "isBetween", _Ex.Message );
        }
        return false;
    }

    public static bool isCellNo( string _sVal )
    {
        try {
            if ( !double.TryParse( _sVal.Trim().Replace( " ", "" ), out double _dParseTo ) ) {
                return false;
            }
            return _sVal.Length == 12;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Valid", "isPhone", _Ex.Message );
        }
        return false;
    }

    public static bool isTelNo( string _sVal )
    {
        try {
            if ( !double.TryParse( _sVal.Trim().Replace( " ", "" ), out double _dParseTo ) ) {
                return false;
            }
            return _sVal.Length <= 12;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Valid", "isTelNo", _Ex.Message );
        }
        return false;
    }
}

public enum Browser
{
    firefox = 1,
    ff = 2,
    internet_explorer = 3,
    ie = 4,
    chrome = 5,
    internet_explorer_7 = 6,
    ie_7 = 7,
    internet_explorer_8 = 8,
    ie_8 = 9,
    internet_explorer_9 = 10,
    ie_9 = 11,
    safari = 12,
    opera = 13,
    none = 14
}

public enum H5InputType
{
    color = 1,
    time = 2,
    date = 3,
    datetime = 4,
    datetime_local = 5,
    email = 6,
    month = 7,
    number = 8,
    range = 9,
    search = 10,
    tel = 11,
    url = 12,
    week = 13
}