﻿public class SessionUserMdl {
    public SessionUserMdl() {
    }

    public SessionUserMdl( string _sUserKey, string _sEmail, string _sFullname, string _sFirstName, AccountType _AccountType ) {
        this._sUserKey = _sUserKey;
        this._sEmail = _sEmail;
        this._sFullname = _sFullname;
        this._sFirstName = _sFirstName;
        this._AccountType = _AccountType;
    }

    #region Properties
    private string _sUserKey;
    private string _sEmail;
    private string _sFullname;
    private string _sFirstName;
    private AccountType _AccountType;

    public string UserKey { get => _sUserKey; set => _sUserKey = value; }
    public string Email { get => _sEmail; set => _sEmail = value; }
    public string Fullname { get => _sFullname; set => _sFullname = value; }
    public string FirstName { get => _sFirstName; set => _sFirstName = value; }
    public AccountType AccountType { get => _AccountType; set => _AccountType = value; }
    #endregion
}
