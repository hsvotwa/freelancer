﻿using MySql.Data.MySqlClient;
using System;
using System.Data;

public class SqlClass {
    public static string cleanValue( string _sValue ) {
        try {
            return _sValue.Replace( "'", "\\'" );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "SQL", "cleanValue", _Ex.Message );
        }
        return _sValue;
    }

    public bool isRecordExist( string _sTable, string _sExtraQuery = "" ) {
        try {
            string _sQuery = "";
            _sQuery = "Select * from " + _sTable + " ";
            _sQuery += _sExtraQuery;
            return numRow( _sQuery ) > 0;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "SQL", "isRecordExist", _Ex.Message );
        }
        return false;
    }

    public string getSingleItem( string _sTable, string _sColumn, string _sExtraQuery = "" ) {
        try {
            string _sSelectQuery = "";
            _sSelectQuery = "select " + _sColumn + " as item from " + _sTable + " ";
            _sSelectQuery += " " + _sExtraQuery;
            _sSelectQuery += " LIMIT 1";
            DataRow _Dr = getDataRow( _sSelectQuery );
            if ( _Dr == null ) {
                return String.Empty;
            }
            return _Dr[_sColumn].ToString();
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "SQL", "getSingleItem", _Ex.Message );
        }
        return String.Empty;
    }

    public string getSingleItem( string _sSelectQuery = "" ) {
        try {
            DataRow _Dr = getDataRow( _sSelectQuery );
            if ( _Dr == null ) {
                return String.Empty;
            }
            return _Dr[0].ToString();
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "SQL", "getSingleItem", _Ex.Message );
            return "";
        }
    }

    public static int numRow( string _sTable, string _sColumn, string _sCondition = "" ) {
        int _iReturn = 0;
        MySqlConnection g_Con = new MySqlConnection( Constant.CON_STRING );
        try {
            if ( g_Con.State != ConnectionState.Open ) {
                g_Con.Open();
            }
            if ( g_Con.State != ConnectionState.Open ) {
                return _iReturn;
            }
            string _sCmdText = "select ifnull(count(" + _sColumn + "), 0) as count from " + _sTable + " " + _sCondition;
            MySqlDataAdapter _SqlDataAdapter = new MySqlDataAdapter( _sCmdText, g_Con );
            DataSet _DataSet = new DataSet();
            _SqlDataAdapter.Fill( _DataSet, "NoRow" );
            return int.Parse( _DataSet.Tables[0].Rows[0]["count"].ToString() );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "SQL", "numRow", _Ex.Message );
        } finally {
            if ( g_Con.State == ConnectionState.Open ) {
                g_Con.Close();
            }
        }
        return _iReturn;
    }

    public static int numRow( string _sCmdText ) {
        int _iReturn = 0;
        MySqlConnection g_Con = new MySqlConnection( Constant.CON_STRING );
        try {
            if ( g_Con.State != ConnectionState.Open ) {
                g_Con.Open();
            }
            if ( g_Con.State != ConnectionState.Open ) {
                return _iReturn;
            }
            MySqlDataAdapter _SqlDataAdapter = new MySqlDataAdapter( _sCmdText, g_Con );
            DataSet _DataSet = new DataSet();
            _SqlDataAdapter.Fill( _DataSet, "NoRow" );
            _iReturn = _DataSet.Tables[0].Rows.Count;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "SQL", "numRow", _Ex.Message );
        } finally {
            if ( g_Con.State == ConnectionState.Open ) {
                g_Con.Close();
            }
        }
        return _iReturn;
    }

    public static DataSet getDataSet( string _sCmdText ) {
        DataSet _dsReturn = new DataSet();
        MySqlConnection g_Con = new MySqlConnection( Constant.CON_STRING );
        try {
            if ( g_Con.State != ConnectionState.Open ) {
                g_Con.Open();
            }
            if ( g_Con.State != ConnectionState.Open ) {
                return null;
            }
            MySqlDataAdapter _SqlDataAdapter = new MySqlDataAdapter( _sCmdText, g_Con );
            _SqlDataAdapter.Fill( _dsReturn, "DataSet" );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "SQL", "getDataSet", _Ex.Message );
        } finally {
            if ( g_Con.State == ConnectionState.Open ) {
                g_Con.Close();
            }
        }
        return _dsReturn;
    }

    public static DataRow getDataRow( string _sCmdText ) {
        DataRow _drReturn = null;
        MySqlConnection g_Con = new MySqlConnection( Constant.CON_STRING );
        try {
            if ( g_Con.State != ConnectionState.Open ) {
                g_Con.Open();
            }
            if ( g_Con.State != ConnectionState.Open ) {
                return null;
            }
            MySqlDataAdapter _SqlDataAdapter = new MySqlDataAdapter( _sCmdText, (MySqlConnection)g_Con );
            DataSet _DataSet = new DataSet();
            _SqlDataAdapter.Fill( _DataSet, "DataSet" );

            if ( _DataSet.Tables[0].Rows.Count > 0 ) {
                _drReturn = _DataSet.Tables[0].Rows[0];
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "SQL", "getDataRow", _Ex.Message );
        } finally {
            if ( g_Con.State == ConnectionState.Open ) {
                g_Con.Close();
            }
        }
        return _drReturn;
    }

    public static bool canCon() {
        MySqlConnection g_Con = new MySqlConnection( Constant.CON_STRING );
        try {
            if ( g_Con.State != ConnectionState.Open ) {
                g_Con.Open();
            }
            if ( g_Con.State != ConnectionState.Open ) {
                return false;
            }
            return g_Con.State == ConnectionState.Open;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "SQL", "execCmdText", _Ex.Message );
        } finally {
            if ( g_Con.State == ConnectionState.Open ) {
                g_Con.Close();
            }
        }
        return false;
    }

    public static bool execCmdText( string _sCmdText ) {
        MySqlConnection g_Con = new MySqlConnection( Constant.CON_STRING );
        try {
            if ( g_Con.State != ConnectionState.Open ) {
                g_Con.Open();
            }
            if ( g_Con.State != ConnectionState.Open ) {
                return false;
            }
            MySqlCommand _SqlCmd = g_Con.CreateCommand();
            _SqlCmd.CommandTimeout = 240;
            _SqlCmd.CommandText = _sCmdText;
            return _SqlCmd.ExecuteNonQuery() > 0;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "SQL", "execCmdText", _Ex.Message );
        } finally {
            if ( g_Con.State == ConnectionState.Open ) {
                g_Con.Close();
            }
        }
        return false;
    }
}