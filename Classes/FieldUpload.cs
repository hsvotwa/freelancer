﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.Data;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Field class
/// </summary>

/*
 * MSSQL
 * Here's an example of how the structure of the audit trail to be used, should look like:
 * 
 * CREATE TABLE [dbo].[tbl_u_audit_trail](
    uuid [uniqueidentifier] NOT NULL,
    link_uuid [uniqueidentifier] NOT NULL,
    user_uuid [uniqueidentifier] NOT NULL,
    [description] [varchar](200) NULL,
    [old_value] [varchar](200) NULL,
    [new_value] [varchar](200) NULL,
    column_name [varchar](50) NULL,
    [table_name] [varchar](50) NOT NULL,
    reference_table_name [varchar](50) NULL,
    [version] [int] NOT NULL,
    [created] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_u_audit_trail] PRIMARY KEY CLUSTERED 
(
    uuid ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_u_audit_trail] ADD  CONSTRAINT [DF_tbl_u_audit_trail_guid]  DEFAULT (newid()) FOR uuid
GO

ALTER TABLE [dbo].[tbl_u_audit_trail] ADD  CONSTRAINT [DF_tbl_u_audit_trail_created]  DEFAULT (now()) FOR [created]
GO
 */

//MySQL
/*
 * CREATE TABLE `tbl_u_audit_trail` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `uuid` char(36) NOT NULL,
      `link_uuid` char(36) NOT NULL,
      `user_uuid` char(36) NOT NULL,
      `description` varchar(500) DEFAULT NULL,
      `old_value` varchar(500) DEFAULT NULL,
      `new_value` varchar(500) DEFAULT NULL,
      `column_name` varchar(40) DEFAULT NULL,
      `table_name` varchar(40) NOT NULL,
      `reference_table_name` varchar(40) DEFAULT NULL,
      `version` int(11) DEFAULT NULL,
      `created` datetime NOT NULL,
      PRIMARY KEY (`id`,`uuid`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
*/

public class Field {
    public const string HTML_FIELD = "html_field";
    public const string SQL_FIELD = "sql_field";
    public const string DESC = "description";
    public const string COMPULSORY = "comp";
    public const string CONV_TO = "conv_to";
    public const string SQL_TBL = "sql_tbl";
    public const string SQL_REF_TBL = "sql_ref_tbl";
    public const string OLD_VALUE = "old_value";
    public const string NEW_VALUE = "new_value";
    public const string VALID = "valid";  //Determine"s if a field has been validated and if it's valid or not
    public const string EXCL_TRACK = "excl_track";  //Determine's whether a field should be excluded when tracking (audit trail) is done
    public const string PLACEHOLDER = "placeholder";

    private Database g_SqlDb = null;
    private string g_sLnkGuid = "";
    private string g_sUsrGuid = "";
    private List<List<ListItem>> g_lFields = new List<List<ListItem>>();
    private List<ListItem> g_lField = null;
    private DynamicRecord g_CompRow = null;
    private string g_sAuditTrailTbl = "tbl_u_audit_trail";  //This is the SQL audit trail table which will be used for read & write of data
    private bool g_bIsNew = false;
    private bool g_bIsPost = true;
    private dynamic g_AllFieldValid = null;
    //private bool g_bNoDefOption = false;

    public Database SqlDb {
        get {
            if ( this.g_SqlDb == null ) {
                this.g_SqlDb = Database.OpenConnectionString( Constant.CON_STRING, Constant.SQL_PROVIDER );
            }

            return this.g_SqlDb;
        }

        set {
            this.g_SqlDb = value;
        }
    }

    public string LnkGuid {
        get {
            return this.g_sLnkGuid;
        }

        set {
            this.g_sLnkGuid = value.Trim().ToLower();
        }
    }

    public string UsrGuid {
        get {
            return this.g_sUsrGuid;
        }

        set {
            this.g_sUsrGuid = value.Trim().ToLower();
        }
    }

    public List<List<ListItem>> Fields {
        get {
            return this.g_lFields;
        }

        set {
            this.g_lFields = value;
        }
    }

    public List<ListItem> ChildField {
        get {
            if ( this.g_lField == null ) {
                this.g_lField = new List<ListItem>();
            }

            return this.g_lField;
        }

        set {
            this.g_lField = value;
        }
    }

    public DynamicRecord CompRow {
        get {
            return this.g_CompRow;
        }

        set {
            this.g_CompRow = value;
        }
    }

    public string AuditTrailTbl {
        get {
            return this.g_sAuditTrailTbl;
        }

        set {
            this.g_sAuditTrailTbl = value.Trim().ToLower();
        }
    }

    private bool IsNew {
        get {
            return this.g_bIsNew;
        }

        set {
            this.g_bIsNew = value;
        }
    }

    public bool IsPost {
        get {
            return this.g_bIsPost;
        }

        set {
            this.g_bIsPost = value;
        }
    }

    public bool AllFieldValid {
        get {
            if ( this.g_AllFieldValid == null ) {
                this.g_AllFieldValid = this.isFieldValid();
            }

            return ( bool )this.g_AllFieldValid;
        }

        set {
            this.g_AllFieldValid = ( bool )value;
        }
    }

    public Field() {
        //
    }

    public Field( string _sLnkGuid, string _sUsrGuid ) {
        try {
            this.LnkGuid = _sLnkGuid;
            this.UsrGuid = _sUsrGuid;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "Field", _Ex.Message );
        }
    }

    public Field( string _sLnkGuid, string _sUsrGuid, List<List<ListItem>> _lFields ) {
        try {
            this.LnkGuid = _sLnkGuid;
            this.UsrGuid = _sUsrGuid;
            this.Fields = _lFields;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "Field", _Ex.Message );
        }
    }

    public bool trackChgs() {
        string _sHtmlValue = "", _sSqlValue = "", _sQry = "";
        string _sSqlField;
        bool _bReturn = true, _bExclTrackChg;

        try {
            if ( this.UsrGuid == String.Empty ) {
                return _bReturn;
            }

            this.IsNew = this.CompRow == null;

            if ( this.LnkGuid == String.Empty
                    || !this.IsNew
                    && this.CompRow == null ) {
                return _bReturn;
            }

            /*** BEGIN - Loop through fields to tracked ***/
            foreach ( List<ListItem> _lField in this.Fields ) {
                if ( !Common.inListItem( _lField, EXCL_TRACK, out _bExclTrackChg )  //Check if the field has been set to be excluded from tracking (audit trail)
                        || !_bExclTrackChg ) {

                    if ( this.fieldExists( Common.getListItemValue( _lField, HTML_FIELD ) ) ) {
                        _sQry = "";
                        _sHtmlValue = this.getHtmlValue( _lField );

                        if ( !this.IsNew )  //This is not a new record/element that's being tracked
                        {
                            //Ensure that the specified SQL column/field exists in the SQL row.
                            //Please note that here we do a bunch of checks because, if the SQL
                            //value is NULL, it caused unexpected results. These checks, however,
                            //resolves the unexpected results and changes are tracked as expected.
                            if ( Common.inListItem( _lField, SQL_FIELD, out _sSqlField )
                                    && this.CompRow.Columns.Contains( _sSqlField )
                                    && this.CompRow[_sSqlField] != null ) {
                                _sSqlValue = this.getSqlValue( _lField );

                                if ( _sHtmlValue != _sSqlValue ) {
                                    _sQry = this.getSqlQry( _lField, _sHtmlValue, _sSqlValue );
                                }
                            } else {
                                //Only track fields that are set
                                if ( _sHtmlValue != String.Empty
                                        && _sHtmlValue != "0" ) {
                                    _sQry = this.getSqlQry( _lField, _sHtmlValue );
                                }
                            }
                        } else  //This is a new record/element that's being tracked
                          {
                            //Only track fields that are set
                            if ( _sHtmlValue != String.Empty
                                    && _sHtmlValue != "0" ) {
                                _sQry = this.getSqlQry( _lField, _sHtmlValue );
                            }
                        }
                        //Execute query
                        if ( _sQry.Trim() != String.Empty ) {
                            if ( this.SqlDb.Execute( _sQry ) != 1
                                    && _bReturn ) {
                                _bReturn = false;
                            }
                        }
                    }
                }
            }
            /*** END - Loop through fields to tracked ***/
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "trackChgs", _Ex.Message );
        }
        return _bReturn;
    }

    public bool trackChg( List<ListItem> _lField = null ) {
        try {
            if ( _lField == null ) {
                if ( this.ChildField.Count == 0 ) {
                    return false;
                }

                _lField = this.ChildField;
            }

            if ( this.UsrGuid == String.Empty
                    || this.LnkGuid == String.Empty ) {
                return false;
            }

            this.IsNew = true;
            return this.SqlDb.Execute( this.getSqlQry( _lField, "", "", false ) ) == 1;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "trackChg", _Ex.Message );
        }

        return false;
    }

    private bool fieldExists( string _sField ) {
        try {
            if ( this.IsPost ) {
                return HttpContext.Current.Request.Form[_sField] != null;
            } else {
                return HttpContext.Current.Request.QueryString[_sField] != null;
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "fieldExists", _Ex.Message );
        }

        return false;
    }

    private string getHtmlValue( List<ListItem> _lField, bool _bToSql = false, bool _bSkipSingleApos = false ) {
        string _sValue = "", _sHtmlField = "";

        try {
            _sHtmlField = Common.getListItemValue( _lField, HTML_FIELD );

            if ( this.fieldExists( _sHtmlField ) ) {
                _sValue = this.convValue( (
                            this.IsPost
                            ? HttpContext.Current.Request.Form[_sHtmlField]
                            : HttpContext.Current.Request.QueryString[_sHtmlField]
                        ).ToString().Trim(),
                        _bToSql,
                        Common.getListItemValue( _lField, CONV_TO ),
                        _bSkipSingleApos
                    );
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "getHtmlValue", _Ex.Message );
        }

        return _sValue;
    }

    private string getSqlValue( List<ListItem> _lField, bool _bToSql = false ) {
        string _sValue = "", _sSqlField;

        try {
            if ( Common.inListItem( _lField, SQL_FIELD, out _sSqlField ) ) {
                if ( this.CompRow.Columns.Contains( _sSqlField ) ) {
                    _sValue = this.convValue(
                                this.CompRow[_sSqlField].ToString().Trim(),
                                _bToSql,
                                Common.getListItemValue( _lField, CONV_TO )
                            );
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "getSqlValue", _Ex.Message );
        }
        return _sValue;
    }

    private string convValue( string _sValue, bool _bToSql, dynamic _ConvTo = null, bool _bSkipSingleApos = false ) {
        try {
            switch ( _ConvTo != null
                        ? ( ConvTo )_ConvTo
                        : ConvTo.none ) {
                case ConvTo.date:
                    //_sValue = Conv.toDate(_sValue, _bToSql, true);
                    _sValue = Conv.toDate( _sValue, false, true );
                    break;
                case ConvTo.date_time:
                    _sValue = Conv.toDate( _sValue, _bToSql, false );
                    break;
                case ConvTo.integer:
                    _sValue = Conv.toNum( _sValue, _bToSql, 0 );
                    break;
                case ConvTo._double:
                case ConvTo._float:
                    _sValue = Conv.toNum( _sValue, _bToSql, 2 );
                    break;
            }

            //Convert the value to lower case if it's a GUID value.
            //This is necessary because SQL represents and stores 
            //GUIDs in upper case by default, why, when the data is 
            //retrieved in C#/Razor, it's converted to lower case. Naturally,
            //will will result in value being tracked incorrectly.
            if ( Validate.isGuid( _sValue ) ) {
                _sValue = _sValue.ToLower();
            }

            if ( _bToSql
                    && !_bSkipSingleApos ) {
                _sValue = "'" + _sValue.Replace( "'", "''" ) + "'";
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "convValue", _Ex.Message );
        }
        return _sValue;
    }

    private string getSqlQry( List<ListItem> _lField, string _sHtmlValue = "", string _sSqlValue = "", bool _bMulti = true ) {
        string _sQry = "", _sQryCol = "", _sQryVal = "";
        string _sSqlField = "", _sValue = "";
        int _iVersion = 1;

        try {
            /*** BEGIN - Determine version ***/
            if ( Common.inListItem( _lField, SQL_FIELD, out _sSqlField ) ) {
                if ( !this.IsNew
                        && this.CompRow.Columns.Contains( _sSqlField ) )  //Ensure that the specified sql column exists in the SQL row
                {
                    _sQry = "select ";
                    _sQry += "version ";
                    _sQry += "from " + this.AuditTrailTbl + " ";
                    _sQry += "where (link_uuid='" + this.LnkGuid + "') ";
                    _sQry += "and (column_name='" + _sSqlField + "') ";
                    _sQry += "order by created desc;";
                    var _vRow = this.SqlDb.QuerySingle( _sQry );

                    if ( _vRow != null ) {
                        _iVersion = int.Parse( _vRow["version"].ToString() ) + 1;
                    }
                }
            }
            /*** END - Determine version ***/

            _sQry = "insert into " + this.AuditTrailTbl + " ";
            _sQryCol = "(";
            _sQryVal = "values (";
            _sQryCol += "uuid, ";
            _sQryVal += "uuid(), ";
            _sQryCol += "link_uuid, ";
            _sQryVal += this.convValue( this.LnkGuid, true ) + ", ";
            _sQryCol += "user_uuid, ";
            _sQryVal += this.convValue( this.UsrGuid, true ) + ", ";
            _sQryCol += "version, ";
            _sQryVal += _iVersion.ToString() + ", ";

            if ( _bMulti ) {
                if ( !this.IsNew ) {
                    _sQryCol += "" + OLD_VALUE + ", ";
                    _sQryVal += this.convValue( _sSqlValue, true ) + ", ";
                }

                _sQryCol += "" + NEW_VALUE + ", ";
                _sQryVal += this.convValue( _sHtmlValue, true ) + ", ";
                _sQryCol += "column_name, ";
                _sQryVal += this.convValue( _sSqlField, true ) + ", ";
                _sQryCol += "table_name, ";
                _sQryVal += this.convValue( Common.getListItemValue( _lField, SQL_TBL ), true ) + ", ";

                if ( Common.inListItem( _lField, SQL_REF_TBL, out _sValue ) ) {
                    _sQryCol += "reference_table_name, ";
                    _sQryVal += this.convValue( _sValue, true ) + ", ";
                }

                _sQryCol += "description, ";
                _sQryVal += this.convValue( Common.getListItemValue( _lField, DESC ), true ) + ", ";
            } else  //Log a single value. An example of this is a password change, for exampe
              {
                if ( Common.inListItem( _lField, DESC, out _sValue ) ) {
                    _sQryCol += "" + DESC + ", ";
                    _sQryVal += this.convValue( _sValue, true ) + ", ";
                }

                if ( Common.inListItem( _lField, OLD_VALUE, out _sValue ) ) {
                    _sQryCol += "" + OLD_VALUE + ", ";
                    _sQryVal += this.convValue( _sValue, true ) + ", ";
                }

                if ( Common.inListItem( _lField, NEW_VALUE, out _sValue ) ) {
                    _sQryCol += "" + NEW_VALUE + ", ";
                    _sQryVal += this.convValue( _sValue, true ) + ", ";
                }

                if ( Common.inListItem( _lField, SQL_FIELD, out _sValue ) ) {
                    _sQryCol += "column_name, ";
                    _sQryVal += this.convValue( _sValue, true ) + ", ";
                }

                if ( Common.inListItem( _lField, SQL_TBL, out _sValue ) ) {
                    _sQryCol += "table_name, ";
                    _sQryVal += this.convValue( _sValue, true ) + ", ";
                }

                if ( Common.inListItem( _lField, SQL_REF_TBL, out _sValue ) ) {
                    _sQryCol += "reference_table_name, ";
                    _sQryVal += this.convValue( _sValue, true ) + ", ";
                }
            }

            _sQryCol += "created";
            _sQryVal += "now()";
            _sQryCol += ") ";
            _sQryVal += ")";
            _sQry += _sQryCol + _sQryVal + ";";
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "getSqlQry", _Ex.Message );
        } finally {
            if ( this.SqlDb.Connection.State == System.Data.ConnectionState.Open ) {
                this.SqlDb.Close();
            }
        }
        return _sQry.Trim();
    }

    public bool validate( List<List<ListItem>> _lFields = null, bool _bValidAll = true ) {
        string _sHtmlField = "", _sValue = "", _sPlaceholder;
        bool _bReturn = true, _bComp = false;
        int _iCount = 0;
        List<ListItem> _lField = null;
        double _ddChk;

        try {
            if ( _lFields != null ) {
                this.Fields = _lFields;
            }

            _iCount = this.Fields.Count;

            for ( int _iIndex = 0; _iIndex < _iCount; _iIndex++ ) {
                _lField = this.Fields[_iIndex];

                //Ensure that field exists (either in Post or Get data, as setup)
                if ( Common.inListItem( _lField, HTML_FIELD, out _sHtmlField )
                        && this.fieldExists( _sHtmlField ) ) {
                    _sValue = this.getHtmlValue( _lField, true, true );

                    if ( Common.inListItem( _lField, COMPULSORY, out _bComp )  //Field is compulsory, do all validations
                            && _bComp ) {
                        if ( _sValue == String.Empty
                                || ( Double.TryParse( _sValue, out _ddChk ) && _ddChk == 0 )
                                || !this.validFieldType( _sValue, Common.getListItemValue( _lField, CONV_TO ) )
                                || Common.inListItem( _lField, PLACEHOLDER, out _sPlaceholder ) && _sValue == _sPlaceholder )  //Field is not compulsory, only ensure type validation and value is not = placeholder (if set)
                        {
                            if ( _bValidAll ) {
                                this.updFieldIndexValue( _iIndex, VALID, false );
                                if ( _bReturn ) {
                                    _bReturn = false;
                                }
                            } else {
                                _bReturn = false;
                                break;
                            }
                        }
                    } else  //Field is not compulsory, only ensure type validation and value is not = placeholder (if set)
                      {
                        if ( !this.validFieldType( _sValue, Common.getListItemValue( _lField, CONV_TO ) )
                                || Common.inListItem( _lField, PLACEHOLDER, out _sPlaceholder ) && _sValue == _sPlaceholder ) {
                            if ( _bValidAll ) {
                                this.updFieldIndexValue( _iIndex, VALID, false );
                                if ( _bReturn ) {
                                    _bReturn = false;
                                }
                            } else {
                                _bReturn = false;
                                break;
                            }
                        }
                    }
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "validate", _Ex.Message );
        }
        return _bReturn;
    }

    private bool validFieldType( string _sValue, dynamic _ConvTo ) {
        try {
            switch ( _ConvTo != null
                        ? ( ConvTo )_ConvTo
                        : ConvTo.none ) {
                case ConvTo.date:
                    return Validate.isDate( _sValue );
                case ConvTo.date_time:
                    DateTime _dtResult;
                    return DateTime.TryParse( _sValue, out _dtResult );
                case ConvTo.integer:
                    int _iResult;
                    return int.TryParse( Conv.toNum( _sValue, true ), out _iResult );
                case ConvTo._double:
                    double _ddResult;
                    return double.TryParse( Conv.toNum( _sValue, true ), out _ddResult );
                case ConvTo._float:
                    float _fResult;
                    return float.TryParse( Conv.toNum( _sValue, true ), out _fResult );
                case ConvTo.email:
                    return Validate.isEmail( _sValue );
                default:  //string, none
                    return true;
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "validFieldType", _Ex.Message );
        }

        return false;
    }

    private bool updFieldIndexValue( int _iIndex, string _sIndex, dynamic _Value ) {
        int _iRefIndex;

        try {
            List<ListItem> _lField = this.Fields[_iIndex];

            if ( Common.inListItem( _lField, _sIndex, out _iRefIndex ) )  //Update existing value
            {
                _lField[_iRefIndex].setProperty( _sIndex, _Value );
            } else  //Add parameter if not found
              {
                _lField.Add( new ListItem( _sIndex, _Value ) );
            }

            this.Fields[_iIndex] = _lField;
            return true;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "updFieldIndexValue", _Ex.Message );
        }

        return false;
    }

    public bool updFieldIndexValue( string _sField, string _sIndex, dynamic _Value ) {
        int _iRefIndex, _iCount = 0;
        List<ListItem> _lField = null;

        try {
            _iCount = this.Fields.Count;

            for ( int _iIndex = 0; _iIndex < _iCount; _iIndex++ ) {
                _lField = this.Fields[_iIndex];

                if ( Common.inListItem( _lField, HTML_FIELD, out _iRefIndex )
                        && _lField[_iRefIndex].getProperty( HTML_FIELD ) == _sField ) {
                    if ( Common.inListItem( _lField, _sIndex, out _iRefIndex ) )  //Update existing value
                    {
                        _lField[_iRefIndex].setProperty( _sIndex, _Value );
                    } else  //Add parameter if not found
                      {
                        _lField.Add( new ListItem( _sIndex, _Value ) );
                    }

                    this.Fields[_iIndex] = _lField;
                    return true;
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "updFieldIndexValue", _Ex.Message );
        }

        return false;
    }

    private bool isFieldValid() {
        int _iRefIndex;

        try {
            if ( this.IsPost && !IsPost ) { return true; }

            foreach ( List<ListItem> _lField in this.Fields ) {
                if ( Common.inListItem( _lField, VALID, out _iRefIndex ) ) {
                    if ( !( ( bool )_lField[_iRefIndex].getProperty( VALID ) ) ) {
                        return false;
                    }
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "isFieldValid", _Ex.Message );
        }

        return true;
    }

    public bool isFieldValid( string _sField ) {
        int _iRefIndex;

        try {
            foreach ( List<ListItem> _lField in this.Fields ) {
                if ( Common.inListItem( _lField, HTML_FIELD, out _iRefIndex )
                        && ( string )_lField[_iRefIndex].getProperty( HTML_FIELD ) == _sField ) {
                    if ( Common.inListItem( _lField, VALID, out _iRefIndex ) ) {
                        return ( bool )_lField[_iRefIndex].getProperty( VALID );
                    }

                    return true;
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "isFieldValid", _Ex.Message );
        }

        return true;
    }

    public void addChildField() {
        try {
            if ( this.g_lField != null
                    && this.g_lField.Count > 0 ) {
                this.Fields.Add( this.ChildField );
            }

            this.ChildField = null;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "addChildField", _Ex.Message );
        }
    }

    public void addChildField( string _sDesc, string _sHtmlField, string _sSqlField, string _sSqlTbl, ConvTo _ConvTo = ConvTo._string, bool _bIsComp = false, string _sSqlRefTbl = "", string _sPlaceholder = "", bool _bExclTrackChg = false ) {
        try {
            this.ChildField.Add( new ListItem( DESC, _sDesc ) );
            this.ChildField.Add( new ListItem( HTML_FIELD, _sHtmlField ) );
            this.ChildField.Add( new ListItem( SQL_FIELD, _sSqlField ) );
            this.ChildField.Add( new ListItem( SQL_TBL, _sSqlTbl ) );
            this.ChildField.Add( new ListItem( CONV_TO, _ConvTo ) );
            this.ChildField.Add( new ListItem( COMPULSORY, _bIsComp ) );

            if ( _sSqlRefTbl.Trim() != String.Empty ) {
                this.ChildField.Add( new ListItem( SQL_REF_TBL, _sSqlRefTbl ) );
            }

            if ( _sPlaceholder.Trim() != String.Empty ) {
                this.ChildField.Add( new ListItem( PLACEHOLDER, _sPlaceholder ) );
            }

            if ( _bExclTrackChg ) {
                this.ChildField.Add( new ListItem( EXCL_TRACK, _bExclTrackChg ) );
            }

            this.addChildField();
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "addChildField", _Ex.Message );
        }
    }

    public string getJsHtmlField( bool _bCompOnly = true ) {
        string _sReturn = "", _sPlaceholder;
        int _iRefIndex;
        bool _bComp;

        try {
            foreach ( List<ListItem> _lField in this.Fields ) {
                if ( Common.inListItem( _lField, HTML_FIELD, out _iRefIndex ) ) {
                    if ( _bCompOnly ) {
                        if ( !Common.inListItem( _lField, COMPULSORY, out _bComp )
                                || _bComp ) {
                            if ( _sReturn.Trim() != String.Empty ) { _sReturn += ","; }
                            _sReturn += _lField[_iRefIndex].getProperty( HTML_FIELD );

                            if ( Common.inListItem( _lField, PLACEHOLDER, out _sPlaceholder ) ) {
                                _sReturn += "|" + _sPlaceholder;
                            }
                        }
                    } else {
                        if ( _sReturn.Trim() != String.Empty ) { _sReturn += ","; }
                        _sReturn += _lField[_iRefIndex].getProperty( HTML_FIELD );

                        if ( Common.inListItem( _lField, PLACEHOLDER, out _sPlaceholder ) ) {
                            _sReturn += "|" + _sPlaceholder;
                        }
                    }
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "getJsHtmlField", _Ex.Message );
        }

        return _sReturn.Trim();
    }

    public string getJsHtmlFieldFocusInvalid( bool _bFieldOnly = false ) {
        int _iRefIndex;
        string _sField = "", _sReturn = "";

        try {
            //Ensure that one of the fields failed valifation,
            //before starting this process.
            if ( this.AllFieldValid ) { return _sReturn; }

            foreach ( List<ListItem> _lField in this.Fields ) {
                _sField = "";

                if ( Common.inListItem( _lField, HTML_FIELD, out _iRefIndex ) ) {
                    _sField = ( string )_lField[_iRefIndex].getProperty( HTML_FIELD );

                    if ( Common.inListItem( _lField, VALID, out _iRefIndex )
                            && !( ( bool )_lField[_iRefIndex].getProperty( VALID ) ) ) {
                        break;
                    }
                }
            }

            if ( _sField.Trim() != String.Empty ) {
                if ( !_bFieldOnly ) {
                    _sReturn = "<script>";
                    _sReturn += "$(function(){\"function\"===typeof changeTab?changeTab(\"{FIELD}\"):\"function\"===typeof chgTab&&chgTab(\"{FIELD}\");$(\"#{FIELD}\").focus()});";
                    _sReturn += "</script>";
                    _sReturn = _sReturn.Replace( "{FIELD}", _sField );
                } else {
                    _sReturn = _sField;
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "getJsHtmlFieldFocusInvalid", _Ex.Message );
        }

        return _sReturn;
    }

    public string getChgLog( string _sRestrictTableName = "") {
        string _sReturn = "", _sQry = "", _sCssClass = "";
        IEnumerable<dynamic> _vResult = null;
        SqlClass _SqlClass = new SqlClass();
        int _iCount = 0;

        try {
            if ( this.LnkGuid == String.Empty ) {
                return _sReturn;
            }
            _sQry = "select at.*, ";
            _sQry += "concat(u.last_name, ' ', u.first_name) as name ";
            _sQry += "from tbl_u_audit_trail at inner join tbl_user u on at.user_uuid=u.uuid ";
            _sQry += "where (at.link_uuid='" + this.LnkGuid + "') ";
            _sQry += _sRestrictTableName == String.Empty ? "" :  "and (at.table_name ='" + _sRestrictTableName + "') ";
            _sQry += "order by created desc;";
            _vResult = this.SqlDb.Query( _sQry );
            if ( _vResult.Count() == 0 ) {
                return "<p class=\"warn_mess\">No audit trail information found.</p>";
            }
            //Create headers in seperate table. We do this, because the division 
            //with the detail will and must be scrollable.
            _sReturn += "<table class=\"table_global\">";
            _sReturn += "<tr>";
            _sReturn += "<td class=\"td_head\" style=\"width:120px;\">"; //Role audit trail table has different dimensions
            _sReturn += "Date";
            _sReturn += "</td>";
            _sReturn += "<td class=\"td_head\" style=\"width: 605px;\">";
            _sReturn += "Action description";
            _sReturn += "</td>";
            _sReturn += "<td class=\"td_head\" style=\"width: 260px;padding-left:0px;\">";
            _sReturn += " Responsible user";
            _sReturn += "</td>";
            _sReturn += "</tr>";
            _sReturn += "</table>";

            //Create division, which will be scrollable with the detail
            _sReturn += "<div class=\"div_audit_trail\">";
            _sReturn += "<table class=\"table_global\">";

            /*** BEGIN - Loop through changes (audit trail records) ***/
            foreach ( var _vRow in _vResult ) {
                _sCssClass = (
                        ++_iCount % 2 == 0
                        ? "td_body"
                        : "td_body_alt"
                    );
                _sReturn += "<tr>";
                _sReturn += "<td class=\"" + _sCssClass + "\" style=\"width: 150px; vertical-align: text-top;\">";
                _sReturn += Conv.toDate( _vRow["created"].ToString() );
                _sReturn += "</td>";
                _sReturn += "<td class=\"" + _sCssClass + "\" style=\"width: 605px; vertical-align: text-top;\">";
                _sReturn += _vRow["description"].ToString(); //Get description.

                if ( _vRow[NEW_VALUE] != null ) {
                    _sReturn += (
                            _vRow["old_value"] != null && _vRow["old_value"] != ""
                            ? " changed from " + this.getChgLogValue( _vRow, OLD_VALUE ) + " to " + this.getChgLogValue( _vRow, NEW_VALUE )
                            : " set to " + this.getChgLogValue( _vRow, NEW_VALUE )
                        );
                }

                if ( _sReturn.Substring( _sReturn.Length - 1, 1 ) != "." ) {
                    _sReturn += ".";
                }
                _sReturn += "</td>";
                _sReturn += "<td class=\"" + _sCssClass + "\" style=\"width: 260px; vertical-align: text-top;\">";
                _sReturn += _vRow["name"].ToString();
                _sReturn += "</td>";
                _sReturn += "</tr>";

            }
            /*** END - Loop through changes (audit trail records) ***/
            _sReturn += "<tr>"; //Extra row for spacing at the bottom
            _sReturn += "<td colspan=\"3\"> &nbsp;";
            _sReturn += "</td>";
            _sReturn += "</tr>";
            _sReturn += "</table>";
            _sReturn += "</div>";
        } catch ( Exception ) {
            //ErrorHandler _Err = new ErrorHandler();
        }
        return _sReturn;
    }

    public string getUserChgLog( string _sUserUuid, DateTime _dtFrom, DateTime _dtTo, out string _sExportQry ) {
        _sExportQry = string.Empty;
        _sUserUuid = _sUserUuid.Trim();
        string _sReturn = "", _sQry = "", _sCssClass = "";
        IEnumerable<dynamic> _vResult = null;
        SqlClass _SqlClass = new SqlClass();
        int _iCount = 0;
        try {
            _sExportQry = "select at.created, at.table_name as changed_on, at.old_value, at.new_value, ";
            _sExportQry += "concat(u.last_name, ' ', u.first_name) as user, at.description ";
            _sExportQry += "from tbl_audit_trail at inner join tbl_user u on at.user_uuid=u.uuid ";
            _sQry += " where 1=1 ";
            if ( _sUserUuid != String.Empty && _sUserUuid != "0" ) {
                _sQry += " and (at.user_uuid ='" + _sUserUuid + "') ";
            }
            _sExportQry += " where (at.user_uuid ='" + _sUserUuid + "') ";
            _sExportQry += " and (at.created between '" + _dtFrom.AddDays( -1 ).ToString( Constant.FMT_MYSQL_DATE ) + "' and '" + _dtTo.AddDays( 1 ).ToString( Constant.FMT_MYSQL_DATE ) + "' ) ";
            _sExportQry += " order by created desc;";

            _sQry = "select at.*, ";
            _sQry += "concat(u.last_name, ' ', u.first_name) as user ";
            _sQry += "from tbl_audit_trail at inner join tbl_user u on at.user_uuid=u.uuid ";
            _sQry += " where 1=1 ";
            if ( _sUserUuid != String.Empty && _sUserUuid != "0" ) {
                _sQry += "and (at.user_uuid ='" + _sUserUuid + "') ";
            }
            _sQry += " and (at.created between '" + _dtFrom.AddDays( -1 ).ToString( Constant.FMT_MYSQL_DATE ) + "' and '" + _dtTo.AddDays( 1 ).ToString( Constant.FMT_MYSQL_DATE ) + "' ) ";
            _sQry += " order by created desc;";
            _vResult = this.SqlDb.Query( _sQry );
            if ( _vResult.Count() == 0 ) {
                return "<p class=\"warn_mess\">No audit trail information found.</p>";
            }
            //Create headers in seperate table. We do this, because the division 
            //with the detail will and must be scrollable.
            _sReturn += "<table class=\"table_global\">";
            _sReturn += "<tr>";
            _sReturn += "<td class=\"td_head\" style=\"width:120px;\">"; //Role audit trail table has different dimensions
            _sReturn += "Date";
            _sReturn += "</td>";
            _sReturn += "<td class=\"td_head\" style=\"width:200px;\">"; //Role audit trail table has different dimensions
            _sReturn += "Change on";
            _sReturn += "</td>";
            _sReturn += "<td class=\"td_head\" style=\"width: 605px;\">";
            _sReturn += "Action description";
            _sReturn += "</td>";
            _sReturn += "<td class=\"td_head\" style=\"width: 260px;padding-left:0px;\">";
            _sReturn += " Responsible user";
            _sReturn += "</td>";
            _sReturn += "</tr>";
            _sReturn += "</table>";

            //Create division, which will be scrollable with the detail
            _sReturn += "<div class=\"div_audit_trail\">";
            _sReturn += "<table class=\"table_global\">";

            /*** BEGIN - Loop through changes (audit trail records) ***/
            foreach ( var _vRow in _vResult ) {
                _sCssClass = (
                        ++_iCount % 2 == 0
                        ? "td_body"
                        : "td_body_alt"
                    );
                _sReturn += "<tr>";
                _sReturn += "<td class=\"" + _sCssClass + "\" style=\"width: 120px; vertical-align: text-top;\">";
                _sReturn += Conv.toDate( _vRow["created"].ToString() );
                _sReturn += "</td>";
                _sReturn += "<td class=\"" + _sCssClass + "\" style=\"width: 200px; vertical-align: text-top;\">";
                _sReturn += Common.getAuditItemDesc( _vRow["table_name"].ToString() );
                _sReturn += "</td>";
                _sReturn += "<td class=\"" + _sCssClass + "\" style=\"width: 605px; vertical-align: text-top;\">";
                _sReturn += _vRow["description"].ToString(); //Get description.

                if ( _vRow[NEW_VALUE] != null ) {
                    _sReturn += (
                            _vRow["old_value"] != null && _vRow["old_value"] != ""
                            ? " changed from " + this.getChgLogValue( _vRow, OLD_VALUE ) + " to " + this.getChgLogValue( _vRow, NEW_VALUE )
                            : " set to " + this.getChgLogValue( _vRow, NEW_VALUE )
                        );
                }
                if ( _sReturn.Substring( _sReturn.Length - 1, 1 ) != "." ) {
                    _sReturn += ".";
                }
                _sReturn += "</td>";
                _sReturn += "<td class=\"" + _sCssClass + "\" style=\"width: 260px; vertical-align: text-top;\">";
                _sReturn += _vRow["user"].ToString();
                _sReturn += "</td>";
                _sReturn += "</tr>";

            }
            /*** END - Loop through changes (audit trail records) ***/
            _sReturn += "<tr>"; //Extra row for spacing at the bottom
            _sReturn += "<td colspan=\"3\"> &nbsp;";
            _sReturn += "</td>";
            _sReturn += "</tr>";
            _sReturn += "</table>";
            _sReturn += "</div>";
        } catch ( Exception ) {
            //ErrorHandler _Err = new ErrorHandler();
        }
        return _sReturn;
    }

    private string getChgLogValue( dynamic _Row, string _sCol ) {
        string _sQry = "";
        try {

            string _sNameColumn = "name";
            string _sKeyColumn = "uuid";
            if ( _Row["reference_table_name"] != null ) {
                //Value selected (not the default value in other words) 
                //& therefore the GUID needs to be be resolved to a 
                //human readable value
                //if (Validate.isGuid(_Row[_sCol].ToString()))
                //{
                string _sRefTable = _Row["reference_table_name"].ToString();
                switch ( _sRefTable ) {
                    case "tbl_file":
                        _sNameColumn = "concat(last_name, ' ', first_name)";
                        break;
                    default:
                        _sNameColumn = "name";
                        break;
                }
                if ( _sRefTable.StartsWith( "tbl_lu" ) ) {
                    _sKeyColumn = "id";
                }
                _sQry = "select ";
                _sQry += "" + _sNameColumn + " as name ";
                _sQry += "from " + _Row["reference_table_name"].ToString() + " ";
                _sQry += "where (" + _sKeyColumn + " = '" + _Row[_sCol].ToString() + "');";
                var _vRow = this.SqlDb.QuerySingle( _sQry );
                if ( _vRow != null ) { return _vRow["name"].ToString().Trim(); } else { return "Not resolved."; }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Field", "getChgLogValue", _Ex.Message );
        }

        return _Row[_sCol].ToString();
    }
}
public enum ConvTo {
    none = 0,
    date = 1,
    date_time = 2,
    integer = 3,
    _string = 4,
    _double = 5,
    _float = 6,
    email = 7,
    _char = 8
}