﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;

public class Conv {
    public static string toNum( string _sValue, bool _bToSql = false, int _iDec = 2, string _sZeroDesc = "0.00" ) {
        try {
            _sValue = _sValue.Trim().Replace( ",", "" ).Replace( " ", "" );
            if ( !_bToSql ) {
                if ( _sValue != String.Empty ) {
                    NumberFormatInfo _NumFrmInfo = new NumberFormatInfo();
                    _NumFrmInfo.NumberDecimalDigits = _iDec;
                    _NumFrmInfo.NumberDecimalSeparator = ".";
                    _NumFrmInfo.NumberGroupSeparator = ",";
                    _sValue = (
                            Double.Parse( _sValue ) != 0
                            ? Double.Parse( _sValue.Trim() ).ToString( "N", _NumFrmInfo )
                            : _sZeroDesc
                        );
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Convert", "toNum", _Ex.Message );
        }
        return _sValue;
    }

    public static string toCamelCase( string _sValue ) {
        string _sReturn = "";
        string[] _sWords = _sValue.Trim().ToLower().Replace( "  ", " " ).Split( ' ' );
        try {
            foreach ( string _sWord in _sWords ) {
                if ( _sWord.Trim() != String.Empty ) {
                    if ( _sReturn.Trim() != String.Empty ) { _sReturn += " "; }
                    _sReturn += _sWord.Substring( 0, 1 ).ToUpper() + _sWord.Substring( 1, _sWord.Length - 1 );
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Convert", "toNum", _Ex.Message );
        }
        return _sReturn;
    }

    public static string appSemicolon( ref string _sValue ) {
        try {
            _sValue = _sValue.Trim();
            if ( _sValue.Substring( _sValue.Length - 1, 1 ) != ";" ) {
                _sValue += ";";
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Convert", "appSemicolon", _Ex.Message );
        }
        return _sValue;
    }

    public static string appSemicolon( string _sValue ) {
        try {
            _sValue = _sValue.Trim();

            if ( _sValue.Substring( _sValue.Length - 1, 1 ) != ";" ) {
                _sValue += ";";
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Convert", "appSemicolon", _Ex.Message );
        }
        return _sValue;
    }

    public static string toDate( string _sValue, bool _bToSql = false, bool _bDateOnly = false, bool _bTimeOnly = false, bool _bLongTime = false, string _sFormat = "" ) {
        string _sDateFrmt = "", _sTimeFrmt = "";
        DateTime _dtDateTime = DateTime.Now;
        try {
            if ( DateTime.TryParse( _sValue, out _dtDateTime ) ) {
                _sDateFrmt = (
                        !_bToSql
                        ? "dd/MM/yyyy" //convert to the format set in configurations
                        : Constant.SQL_DATE_FORMAT_SHORT
                    );
                if ( !_bDateOnly ) {
                    _sTimeFrmt = " ";
                    if ( !_bToSql ) {
                        _sTimeFrmt += (
                                !_bLongTime
                                ? "HH:mm"
                                : Constant.TIME_FORMAT_LONG
                            );
                    } else {
                        _sTimeFrmt += Constant.TIME_FORMAT_LONG;
                    }
                }
                _sValue = _dtDateTime.ToString(
                        _sDateFrmt + _sTimeFrmt,
                        DateTimeFormatInfo.InvariantInfo
                    );
                if ( _sFormat != "" ) {
                    return _dtDateTime.ToString(
                         _sFormat,
                         DateTimeFormatInfo.InvariantInfo
                     );
                }
                if ( _bTimeOnly ) {
                    _sValue = _dtDateTime.ToString( _sTimeFrmt.Trim(),
                      DateTimeFormatInfo.InvariantInfo
                  );
                } else if ( _bDateOnly ) {
                    _sValue = _dtDateTime.ToString( _sDateFrmt,
                      DateTimeFormatInfo.InvariantInfo
                  );
                }
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Convert", "toDate", _Ex.Message );
        }
        if ( _sValue.Length < 10 ) {
            _sValue = ( _sValue.StartsWith( "0" ) ) ? _sValue.Insert( 3, "0" ) : _sValue.Insert( 0, "0" );
        }
        return _sValue;
    }

    public static string toNonHtmlSpecialChar( string _sValue ) {
        try {
            string[] _sOriginalChars = new string[2] { "{AMPERSAND}", "{HASH}" };
            string[] _sConvChars = new string[2] { "&", "#" };
            int _iCount = _sOriginalChars.Length;

            for ( int _iI = 0; _iI < _iCount; _iI++ ) {
                _sValue = _sValue.Replace( _sOriginalChars[_iI], _sConvChars[_iI] );
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Convert", "toNonHtmlSpecialChar", _Ex.Message );
        }
        return _sValue;
    }

    public static string toUpperFirst( string _sValue ) {
        try {
            return char.ToUpper( _sValue[0] ) + _sValue.Substring( 1 );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Convert", "toUpperFirst", _Ex.Message );
        }
        return _sValue;
    }

    public static string clean( string _sValue ) {
        try {
            return _sValue.Trim().Replace( "'", "''" );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Conv", "clean", _Ex.Message );
        }
        return _sValue;
    }

    public static bool deterScheduleInterval( string _sExp, out double _ddValue, ref DateTime _dtLastElapse, int _iTry = 0, int _iMaxTry = 20, int _iPulse = 3000, double _ddMinInterval = 5000 ) {
        int _iExp;
        _ddValue = 0;
        try {
            if ( _iTry == 0 && !Validate.isSchedule( _sExp ) ) {
                return false;
            }
            List<string[]> _lParts = new List<string[]>();
            List<DateTime> _lDateTimes = new List<DateTime>();
            DateTime _dtValue;
            foreach ( string _sValue in _sExp.Split( ' ' ) ) {
                _lParts.Add( _sValue.Split( ',' ) );
            }
            for ( int _iSec = 0; _iSec < _lParts[0].Length; _iSec++ ) { //seconds
                if ( int.TryParse( _lParts[0][_iSec], out _iExp ) ) {
                    _dtValue = new DateTime( DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, _iExp, 0 );
                    if ( _dtValue <= DateTime.Now ) {
                        _dtValue = _dtValue.AddMinutes( 1 );
                    }
                } else {
                    _dtValue = new DateTime( DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0 );
                }
                for ( int _iMin = 0; _iMin < _lParts[1].Length; _iMin++ ) { //minutes
                    if ( int.TryParse( _lParts[1][_iMin], out _iExp ) ) {
                        _dtValue = new DateTime( DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, _iExp, _dtValue.Second, 0 );
                        if ( _dtValue <= DateTime.Now ) {
                            _dtValue = _dtValue.AddHours( 1 );
                        }
                    } else {
                        _dtValue = new DateTime( _dtValue.Year, _dtValue.Month, _dtValue.Day, _dtValue.Hour, _dtValue.Minute, _dtValue.Second, 0 );
                    }
                    for ( int _iHr = 0; _iHr < _lParts[2].Length; _iHr++ ) { //hours
                        if ( int.TryParse( _lParts[2][_iHr], out _iExp ) ) {
                            _dtValue = new DateTime( DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, _iExp, _dtValue.Minute, _dtValue.Second, 0 );
                            if ( _dtValue <= DateTime.Now ) {
                                _dtValue = _dtValue.AddDays( 1 );
                            }
                        } else {
                            _dtValue = new DateTime( _dtValue.Year, _dtValue.Month, _dtValue.Day, _dtValue.Hour, _dtValue.Minute, _dtValue.Second, 0 );
                        }
                        for ( int _iWeekday = 0; _iWeekday < _lParts[3].Length; _iWeekday++ ) { //weekdays
                            if ( int.TryParse( _lParts[3][_iWeekday], out _iExp ) ) {
                                if ( _dtValue.Year != DateTime.Now.Year
                                        || _dtValue.Month != DateTime.Now.Month
                                        || _dtValue.Day != DateTime.Now.Day ) {
                                    _dtValue = new DateTime( DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, _dtValue.Hour, _dtValue.Minute, _dtValue.Second, 0 );
                                }

                                while ( _dtValue.DayOfWeek != (DayOfWeek)_iExp ) {
                                    _dtValue = _dtValue.AddDays( 1 );
                                }
                            }
                            if ( _dtValue > DateTime.Now
                                    && _dtValue > _dtLastElapse
                                    && ( _dtValue - DateTime.Now ).TotalMilliseconds >= _ddMinInterval ) {
                                _lDateTimes.Add( _dtValue );
                            }
                        }
                    }
                }
            }
            if ( _lDateTimes.Count > 0 ) {
                if ( _lDateTimes.Count > 1 ) {
                    _lDateTimes.Sort();
                }
                _dtLastElapse = _lDateTimes[0];
                _ddValue = ( _dtLastElapse - DateTime.Now ).TotalMilliseconds;
            }

            if ( _ddValue <= 0 && ++_iTry <= _iMaxTry ) {
                if ( _iPulse > 0 && _iTry >= 5 ) { //after 5 tries, let's try a desperate approach
                    Thread.Sleep( ( _iTry - 4 ) * _iPulse );
                    if ( _dtLastElapse != DateTime.MinValue ) {
                        _dtLastElapse = DateTime.MinValue;
                    }
                }
                return deterScheduleInterval( _sExp, out _ddValue, ref _dtLastElapse, _iTry, _iMaxTry, _iPulse, _ddMinInterval ); //recursion
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError ( "Class", "Conv", "deterScheduleInterval", _Ex.Message );
        }
        return _ddValue > 0;
    }

    public static string rmDblSpace( string _sValue ) {
        try {
            if ( _sValue != String.Empty ) {
                //return Regex.Replace( _sValue, Constant.REG_EXP_DBL_SPACE, " " );
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Conv", "rmDblSpace", _Ex.Message );
        }

        return _sValue;
    }
}