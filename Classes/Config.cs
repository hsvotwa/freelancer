﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

public class Config
{
    public Config( string _sValue, Configuration _Configuration )
    {
        this._sValue = _sValue;
        this._Configuration = _Configuration;
    }

    #region Properties
    private string _sValue;
    private Configuration _Configuration;
    private static string g_sTableName = "tbl_b_config";

    public Configuration ConfigurationID {
        get {
            return _Configuration;
        }
        set {
            _Configuration = value;
        }
    }

    public string Value {
        get {
            return _sValue;
        }
        set {
            _sValue = value;
        }
    }
    #endregion Properties

    #region Methods
    public static bool exist( int _iConfigId )
    {
        try {
            string _sQuery = @"select * 
                                            from tbl_b_config
                                            where enum_config_id= '" + _iConfigId + "'";
            return SqlClass.numRow( _sQuery ) > 0;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Config", "exist", _Ex.Message );
        }
        return false;
    }

    public static Config get( Configuration _Configuration )
    {
        try {
            string _sQuery = @"select * 
                                                from tbl_b_config
                                                where enum_config_id='" + (int) _Configuration + "'";
            DataRow _Row = SqlClass.getDataRow( _sQuery );
            if ( _Row != null ) {
                return
                    new Config
                    (
                        (_Row["value"] == DBNull.Value) ? string.Empty : _Row["value"].ToString(),
                        (Configuration) int.Parse( _Row["enum_config_id"].ToString() )
                    );
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Config", "get", _Ex.Message );
        }
        return null;
    }

    public static string getValue( Configuration _Configuration )
    {
        try {
            string _sQuery = @"select * 
                                            from tbl_b_config
                                            where enum_config_id='" + (int) _Configuration + "'";
            DataRow _Row = SqlClass.getDataRow( _sQuery );
            if ( _Row != null ) {
                return _Row["value"].ToString();
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Config", "getValue", _Ex.Message );
        }
        return String.Empty;
    }

    public static DataSet getAll()
    {
        try {
            string _sQuery = @"select * 
                                            from tbl_b_config";
            return SqlClass.getDataSet( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Config", "getAll", _Ex.Message );
        }
        return null;
    }

    public bool set()
    {
        try {
            bool _bExist = exist( (int) this.ConfigurationID );
            string _sQuery = String.Empty;
            _sQuery += (_bExist ? "update " : "insert into ") + g_sTableName;
            _sQuery += " set value = '" + this.Value + "',";
            _sQuery += "last_modified = now()";
            _sQuery += (!_bExist ? ", created = now()" : "");
            _sQuery += (!_bExist ? ", enum_config_id = " : " where enum_config_id = ") + "'" + (int) this.ConfigurationID + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Config", "set", _Ex.Message );
        }
        return false;
    }

    public static bool setMany( NameValueCollection _Form )
    {
        try {
            if ( _Form == null ) {
                return false;
            }
            foreach ( Configuration _ConfigVal in Enum.GetValues( typeof( Configuration ) ).Cast<Configuration>() ) {
                string _sVal = String.Empty;
                if ( _Form[((int) _ConfigVal).ToString()] == null ) {
                    continue;
                } else {
                    _sVal = _Form[((int) _ConfigVal).ToString()];
                }
                if ( !new Config( _sVal,
                    _ConfigVal ).set() ) {
                    continue;
                }
            }
            return true;
        } catch ( Exception _Ex ) {
            ErrorHandler.logError( "Class", "Config", "setMany", _Ex.Message );
        }
        return false;
    }
    #endregion Methods
}