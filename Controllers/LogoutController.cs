﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Freelancer.Controllers {
    public class LogoutController : Controller {
        public void Index() {
            UserSession.killUserSession();
            UserSession.redirect( "/home/" );
            return;
        }
    }
}