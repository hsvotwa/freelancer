﻿using System.Web.Mvc;

namespace Freelancer.Controllers
{
    public class ErrorHandlerController : Controller
    {
        public ActionResult error_400()
        {
            return View ();
        }

        public ActionResult error_500()
        {
            return View ();
        }
    }
}