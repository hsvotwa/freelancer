﻿using System;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Freelancer.Classes;
using Freelancer.Models;
using Newtonsoft.Json;

namespace Freelancer.Controllers {
    public class FreelancerEmpController : Controller {
        public ActionResult search() {
            return View();
        }

        public ActionResult detail( int id ) {
            FreelancerEmp _Profile = FreelancerEmp.getObj( id.ToString() );
            return View( _Profile );
        }

        public ActionResult edit() {
            FreelancerEmp _Profile = FreelancerEmp.getObj( UserSession.getCurrentUserKey() );
            return View( _Profile );
        }

        [HttpPost]
        public JsonResult update( dynamic _JsonObj ) {
            ResponseStatus _ResponseStatus = ResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                FreelancerEmp _Profile = FreelancerEmp.getObj( UserSession.getCurrentUserKey() );
                UserClass _User = UserClass.getObj( UserSession.getCurrentUserKey() );
                var _vJson = new JavaScriptSerializer().Serialize( _JsonObj );
                dynamic _Data = JsonConvert.DeserializeObject( _vJson );
                //Begin validations
                if ( _User.Email != _Data["email"].ToString() && UserClass.emailTaken( _Data["email"].ToString() ) ) {
                    return Json( new RequestResponse( ResponseStatus.failed, "The new email address you provided is already taken." ) );
                }
                if ( !String.IsNullOrEmpty( _Data["current_password"].ToString() ) ) {
                    if ( !_User.validPassword( _User.Email, _Data["current_password"].ToString() ) ) {
                        return Json( new RequestResponse( ResponseStatus.failed, "Invalid current password. Leave password blank if you don't want to change it." ) );
                    }
                    if ( _Data["new_password"].ToString().Trim() == String.Empty ) {
                        return Json( new RequestResponse( ResponseStatus.failed, "The new cannot be empty." ) );
                    }
                    if ( _Data["new_password"].ToString() != _Data["confirm_password"].ToString() ) {
                        return Json( new RequestResponse( ResponseStatus.failed, "The passwords fields should have identical text. Leave password blank if you don't want to change it." ) );
                    }
                    _User.Password = _Data["current_password"].ToString();
                }
                //End validations
                _User.Name = _Data["name"].ToString();
                _User.Surname = _Data["surname"].ToString();
                _User.AccountType = int.Parse( _Data["freelancer_radio"].ToString() ) == (int)YesNo.yes ? AccountType.freelancer : AccountType.employer;
                _User.TwoStepVerification = int.Parse( _Data["two_step"].ToString() ) == (int)YesNo.yes;
                _Profile.Bio = _Data["bio"].ToString();
                _Profile.SkillTags = _Data["keyword_value"].ToString().TrimStart(','); //At times a residue comma can be left behind
                _Profile.TagLine = _Data["tag_line"].ToString();
                _Profile.MinHourlyRate = decimal.Parse( _Data["min_hourly_rate"].ToString() );
                if ( _User.set() && _Profile.set() ) {
                    _ResponseStatus = ResponseStatus.success;
                    _sDescription = "Looks good! Profile updated." + ( String.IsNullOrEmpty( _Data["current_password"].ToString() ) ? "" : " Password changed." );
                }
            } catch ( Exception _Ex ) {
                _sDescription = _Ex.Message;
                _ResponseStatus = ResponseStatus.error;
            }
            RequestResponse _RequestResponse = new RequestResponse( _ResponseStatus, _sDescription );
            return Json( _RequestResponse );
        }

        public ActionResult myactivebids() {
            return View();
        }
    }
}