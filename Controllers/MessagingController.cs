﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Freelancer.Controllers
{
    public class MessagingController : Controller
    {
        public ActionResult index()
        {
            return View ();
        }

        public ActionResult read( int id) {
            //mark message as read
            return RedirectToAction( "index" );
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try {
                return RedirectToAction ("Index");
            } catch {
                return View ();
            }
        }
    }
}