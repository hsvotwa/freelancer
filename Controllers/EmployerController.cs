﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Freelancer.Classes;
using Freelancer.Models;
using Newtonsoft.Json;

namespace Freelancer.Controllers
{
    public class EmployerController : Controller
    {
        public ActionResult search() {
            return View();
        }

        public ActionResult detail( int id ) {
            Employer _Company = Employer.getObj( id.ToString() );
            return View( _Company );
        }

        public ActionResult edit() {
            Employer _Profile = Employer.getObj( UserClass.getObj( UserSession.getCurrentUserKey() ).CompanyUuid );
            return View( _Profile );
        }

        [HttpPost]
        public JsonResult update( dynamic _JsonObj ) {
            ResponseStatus _ResponseStatus = ResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                UserClass _User = UserClass.getObj( UserSession.getCurrentUserKey() );
                if ( _User == null ) {
                    return Json( new RequestResponse( ResponseStatus.failed, "Your user information could not be retrieved." ) );
                }
                Employer _Company = Employer.getObj( _User.CompanyUuid );
                var _vJson = new JavaScriptSerializer().Serialize( _JsonObj );
                dynamic _Data = JsonConvert.DeserializeObject( _vJson );
                //Begin validations
                if ( _Company == null ) {
                    return Json( new RequestResponse( ResponseStatus.failed, "Your company information could not be retrieved." ) );
                }
                if ( _User.Email != _Data["email"].ToString() && UserClass.emailTaken( _Data["email"].ToString() ) ) {
                    return Json( new RequestResponse( ResponseStatus.failed, "The new email address you provided is already taken." ) );
                }
                if ( _User.Email != _Data["email"].ToString() && UserClass.emailTaken( _Data["email"].ToString() ) ) {
                    return Json( new RequestResponse( ResponseStatus.failed, "The new email address you provided is already taken." ) );
                }
                if ( !String.IsNullOrEmpty( _Data["current_password"].ToString() ) ) {
                    if ( !_User.validPassword( _User.Email, _Data["current_password"].ToString() ) ) {
                        return Json( new RequestResponse( ResponseStatus.failed, "Invalid current password. Leave password blank if you don't want to change it." ) );
                    }
                    if ( _Data["new_password"].ToString().Trim() == String.Empty ) {
                        return Json( new RequestResponse( ResponseStatus.failed, "The new cannot be empty." ) );
                    }
                    if ( _Data["new_password"].ToString() != _Data["confirm_password"].ToString() ) {
                        return Json( new RequestResponse( ResponseStatus.failed, "The passwords fields should have identical text. Leave password blank if you don't want to change it." ) );
                    }
                    _User.Password = _Data["current_password"].ToString();
                }
                //End validations
                _User.Name = _Data["name"].ToString();
                _User.Surname = _Data["surname"].ToString();
                _User.AccountType = int.Parse( _Data["freelancer_radio"].ToString() ) == (int)YesNo.yes ? AccountType.freelancer : AccountType.employer;
                _User.TwoStepVerification = int.Parse( _Data["two_step"].ToString() ) == (int)YesNo.yes;
                _Company.Detail = _Data["detail"].ToString();
                _Company.TwitterLink = _Data["twitter_link"].ToString();
                _Company.FacebookLink = _Data["facebook_link"].ToString();
                _Company.LinkedinLink = _Data["linkedin_link"].ToString();
                _Company.Location = _Data["location"].ToString();
                _Company.CountryId = _Data["country_id"].ToString();
                _Company.BusinessTags = _Data["keyword_value"].ToString();
                _Company.Name = _Data["company_name"].ToString();
                _Company.WebsiteLink = _Data["website_link"].ToString();
                _Company.TagLine = _Data["tag_line"].ToString();
                if ( _User.set() && _Company.set() ) {
                    _ResponseStatus = ResponseStatus.success;
                    _sDescription = "Looks good! Profile updated." + ( String.IsNullOrEmpty( _Data["current_password"].ToString() ) ? "" : " Password changed." );
                }
            } catch ( Exception _Ex ) {
                _sDescription = _Ex.Message;
                _ResponseStatus = ResponseStatus.error;
            }
            RequestResponse _RequestResponse = new RequestResponse( _ResponseStatus, _sDescription );
            return Json( _RequestResponse );
        }

        public ActionResult myactivebids() {
            return View();
        }
    }
}