﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Freelancer.Classes;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace Freelancer.Controllers
{
    public class RegisterController : Controller { 
        [HttpPost]
        public JsonResult create(dynamic _JsonObj )
        {
            ResponseStatus _ResponseStatus = ResponseStatus.failed;
            string _sDescription = "Data could not be saved. Please retry.";
            try {
                var _vJson = new JavaScriptSerializer().Serialize( _JsonObj );
                dynamic _Data = JsonConvert.DeserializeObject( _vJson );
                if ( UserClass.emailTaken( _Data["email"].ToString() ) ) {
                    return Json( new RequestResponse( ResponseStatus.failed, "The email address you provided is already taken." ) );
                }
                UserClass _User = new UserClass(
                    Guid.NewGuid().ToString(),
                    String.Empty,
                    _Data["name"].ToString(),
                    _Data["surname"].ToString(),
                    _Data["email"].ToString(),
                    int.Parse( _Data.freelancer_radio.ToString() ) == (int)YesNo.yes ? AccountType.freelancer : AccountType.employer,
                    _Data["password"].ToString(),
                    false,
                    Status.none
                    );
                if ( _User.set()) {
                    _ResponseStatus = ResponseStatus.success;
                    _sDescription = "Registration successful. An email has been sent to your mailbox, for your confirmation.";
                }
            } catch (Exception _Ex) {
                _sDescription = _Ex.Message;
                _ResponseStatus = ResponseStatus.error;
            }
            RequestResponse _RequestResponse = new RequestResponse( _ResponseStatus, _sDescription );
            return Json( _RequestResponse );
        }
    }
}