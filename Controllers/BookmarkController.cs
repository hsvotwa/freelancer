﻿using System;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Freelancer.Classes;
using Newtonsoft.Json;

namespace Freelancer.Controllers {
    public class BookmarkController : Controller {
        public ActionResult search() {
            return View();
        }

        [HttpPost]
        public ActionResult create( dynamic _JsonObj ) {
            ResponseStatus _ResponseStatus = ResponseStatus.failed;
            string _sDescription = "Bookmark could not be saved. Please retry.";
            try {
                var _vJson = new JavaScriptSerializer().Serialize( _JsonObj );
                dynamic _Data = JsonConvert.DeserializeObject( _vJson );
                string _sKey = Guid.NewGuid().ToString();
                Bookmark _Bookmark = new Bookmark(
                   _sKey,
                   _Data["link_uuid"].ToString(),
                   UserSession.getCurrentUserKey()
                   );
                if ( _Bookmark.set() ) {
                    _ResponseStatus = ResponseStatus.success;
                    _sDescription = "Bookmark saved.";
                }
            } catch ( Exception _Ex ) {
                _sDescription = _Ex.Message;
                _ResponseStatus = ResponseStatus.error;
            }
            RequestResponse _RequestResponse = new RequestResponse( _ResponseStatus, _sDescription );
            return Json( _RequestResponse );
        }

        [HttpPost]
        public ActionResult delete( dynamic _JsonObj ) {
            ResponseStatus _ResponseStatus = ResponseStatus.failed;
            string _sDescription = "Bookmark could not be deleted. Please retry.";
            try {
                var _vJson = new JavaScriptSerializer().Serialize( _JsonObj );
                dynamic _Data = JsonConvert.DeserializeObject( _vJson );
                Bookmark _Bookmark = Bookmark.getObj( _Data["link_uuid"].ToString() );
                if ( _Bookmark == null ) {
                    return Json( new RequestResponse( ResponseStatus.failed, "Ooops. The bookmark could not be located." ) );
                }
                if ( _Bookmark.unset() ) {
                    _ResponseStatus = ResponseStatus.success;
                    _sDescription = "Bookmark deleted.";
                }
            } catch ( Exception _Ex ) {
                _sDescription = _Ex.Message;
                _ResponseStatus = ResponseStatus.error;
            }
            RequestResponse _RequestResponse = new RequestResponse( _ResponseStatus, _sDescription );
            return Json( _RequestResponse );
        }
    }
}