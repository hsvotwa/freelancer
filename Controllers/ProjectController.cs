﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Freelancer.Classes;
using Freelancer.Models;
using Newtonsoft.Json;

namespace Freelancer.Controllers {
    public class ProjectController : Controller {
        public ActionResult create() {
            return View();
        }

        [HttpPost]
        public ActionResult create( dynamic _JsonObj ) {
            ResponseStatus _ResponseStatus = ResponseStatus.failed;
            string _sDescription = "Information could not be posted. Please retry.";
            try {
                var _vJson = new JavaScriptSerializer().Serialize( _JsonObj );
                dynamic _Data = JsonConvert.DeserializeObject( _vJson );
                string _sKey = Guid.NewGuid().ToString();
                Project _Project = new Project(
                   _sKey,
                   _Data["name"].ToString(),
                  int.Parse( _Data["category_id"].ToString() ),
                   (PaymentBasis)int.Parse( _Data["hourly_price_radio"].ToString() ),
                   "",
                   decimal.Parse( _Data["min_budget"].ToString() ),
                   decimal.Parse( _Data["max_budget"].ToString() ),
                   _Data["description"].ToString(),
                   "",//tags
                   UserSession.getCurrentUserKey(),
                  DateTime.Parse( _Data["expiry_date"].ToString() )
                   );
                if ( _Project.set() ) {
                    _ResponseStatus = ResponseStatus.success;
                    _sDescription = "Project posted successfully. Let the bidding begin!";
                }
            } catch ( Exception _Ex ) {
                _sDescription = _Ex.Message;
                _ResponseStatus = ResponseStatus.error;
            }
            RequestResponse _RequestResponse = new RequestResponse( _ResponseStatus, _sDescription );
            return Json( _RequestResponse );
        }

        public ActionResult detail( int id ) {
            return View();
        }

        public ActionResult search() {
            return View();
        }

        public ActionResult bidders() {
            return View();
        }

        public ActionResult viewmy() {
            return View();
        }
    }
}