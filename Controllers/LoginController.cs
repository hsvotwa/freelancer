﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Freelancer.Classes;
//using Freelancer.Classes;
using Newtonsoft.Json;

namespace Freelancer.Controllers {
    public class LoginController : Controller {
        [HttpPost]
        public JsonResult index( dynamic _JsonObj ) {
            ResponseStatus _ResponseStatus = ResponseStatus.failed;
            string _sDescription = "Invalid email and/or password. Please retry.";
            string _sJsFunc = String.Empty;
            try {
                var _vJson = new JavaScriptSerializer().Serialize( _JsonObj );
                dynamic _Data = JsonConvert.DeserializeObject( _vJson );
                UserClass _User = new UserClass();
                if ( _User.authenticateUser( _Data["email_login"].ToString(), _Data["password_login"].ToString() ) ) {
                    _ResponseStatus = ResponseStatus.success;
                    _sDescription = "Log in successful. Let us take you through.";
                    _sJsFunc = "window.location='/dashboard/';";
                }
            } catch ( Exception _Ex ) {
                _sDescription = _Ex.Message;
                _ResponseStatus = ResponseStatus.error;
            }
            RequestResponse _RequestResponse = new RequestResponse( _ResponseStatus, _sDescription, _sJsFunc );
            return Json( _RequestResponse );
        }
    }
}