﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Freelancer.Controllers
{
    public class ReviewController : Controller
    {
        public ActionResult search() {
            return View();
        }

        [HttpPost]
        public ActionResult create( FormCollection collection ) {
            try {
                return RedirectToAction( "Index" );
            } catch {
                return View();
            }
        }

        [HttpPost]
        public ActionResult delete( int id ) {
            return View();
        }
    }
}