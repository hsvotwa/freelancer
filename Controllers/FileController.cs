﻿using System;
using System.IO;
using System.Web.Mvc;
using Freelancer.Classes;

namespace Freelancer.Controllers {
    public class FileController : Controller {
        [HttpPost]
        public JsonResult create( string id, string type_id ) {
            ResponseStatus _ResponseStatus = ResponseStatus.failed;
            string _sDescription = "File could not be uploaded. Please retry.";
            try {
                foreach ( string _sFile in Request.Files ) {
                    var _sFileName = Path.GetFileName( Request.Files[_sFile].FileName );
                    string _sFileOriginalName = _sFileName;
                    var _vFileExt = Path.GetExtension( Request.Files[_sFile].FileName );
                    string _sName = Path.GetRandomFileName();
                    _sName = _sName.Replace( ".", "" );
                    var _vFileSavePath = System.Web.HttpContext.Current.Server.MapPath( "~/" + Constant.UPLOADS_FOLDER + "/" + _sName + _vFileExt );
                    if ( System.IO.File.Exists( _vFileSavePath ) == true ) {
                        _sName = Path.GetRandomFileName();
                        _sName = _sName.Replace( ".", "" );
                        _vFileSavePath = System.Web.HttpContext.Current.Server.MapPath( "~/" + Constant.UPLOADS_FOLDER + "/" + _sName + _vFileExt );
                        Request.Files[_sFile].SaveAs( _vFileSavePath );
                    } else {
                        Request.Files[_sFile].SaveAs( _vFileSavePath );
                    }
                    FileUpload _File = new FileUpload( Guid.NewGuid().ToString(), id, _sName + _vFileExt, _sFileOriginalName, (FileType)int.Parse( type_id ), Status.active, UserSession.getCurrentUserKey() );
                    if ( _File.set() ) {
                        _ResponseStatus = ResponseStatus.success;
                        _sDescription = "You're set! File was uploaded successfully.";
                    }
                }
            } catch ( Exception _Ex ) {
                _sDescription = "Upload error: " + _Ex.Message;
                _ResponseStatus = ResponseStatus.error;
            }
            RequestResponse _RequestResponse = new RequestResponse( _ResponseStatus, _sDescription );
            return Json( _RequestResponse );
        }

        [HttpPost]
        public ActionResult delete( int id ) {
            return View();
        }
    }
}