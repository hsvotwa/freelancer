﻿
var g_get_method = "GET";
var g_post_method = "POST";
var g_exception_message = "Oops, sorry about that! There was an error while processing your request. Please retry :/";

var RequestType = {
    register_user: 1,
    login: 2,
    post_job: 3,
    post_project: 4,
    freelancer_edit: 5,
    file_upload: 6,
    employer_edit: 7
};

var FieldListType = {
    all: 1,
    required: 2
};

var ResponseStatus = {
    success: 1,
    failed: 2,
    error: 3
};

function toggleLoader(show) {
    console.log(show);
    if (!show) {
        $("#loader_image").css("display", "none");
    } else {
        $("#loader_image").css("display", "block");
    }
}

function uploadOnSelect(type_id, field_name, link_uuid) {
    var files = $("#" + field_name)[0].files;
    if (files.length > 0) {
        console.log(files.length);
        if (window.FormData !== undefined) {
            var data = new FormData();
            for (var x = 0; x < files.length; x++) {
                data.append(files[x].name, files[x]);
            }
            $.ajax({
                type: "POST",
                url: '/file/create/' + link_uuid + "?type_id=" + type_id,
                contentType: false,
                processData: false,
                data: data,
                success: function (result) {
                    handleSuccessResponse(RequestType.file_upload, result);
                    if (field_name === "prof_pic" || field_name === "employer_logo") {
                        // avatarSwitcher();
                    }
                },
                error: function (xhr, status, p3, p4) {
                    var err = "Error " + " " + status + " " + p3 + " " + p4;
                    if (xhr.responseText && xhr.responseText[0] === "{")
                        err = JSON.parse(xhr.responseText).Message;
                    handleErrorResponse();
                }
            });
        } else {
            alert("This browser doesn't support HTML5 file uploads!");
        }
    }
}

function deterReqFieldList(request_type, field_list_type) {
    var field_list = "";
    switch (parseInt(request_type)) {
        case RequestType.register_user:
            field_list = (
                parseInt(field_list_type) === FieldListType.all ?
                    "name,surname,email,freelancer_radio,password" :
                    "name,surname,email,freelancer_radio,password,password_repeat"
            );
            break;
        case RequestType.login:
            field_list = (
                parseInt(field_list_type) === FieldListType.all ?
                    "email_login,password_login" :
                    "*"
            );
            break;
        case RequestType.freelancer_edit:
            field_list = (
                parseInt(field_list_type) === FieldListType.all ?
                    "name,surname,email,freelancer_radio,current_password,bio,new_password,min_hourly_rate,confirm_password,tag_line,two_step,keyword_value" :
                    "name,surname,email,bio,min_hourly_rate,tag_line"
            );
            break;
        case RequestType.post_job:
            field_list = (
                parseInt(field_list_type) === FieldListType.all ?
                    "name,category_id,job_mode_id,min_salary,max_salary,expiry_date,description,location" :
                    "*"
            );
            break;
        case RequestType.post_project:
            field_list = (
                parseInt(field_list_type) === FieldListType.all ?
                    "name,category_id,hourly_price_radio,max_budget,min_budget,expiry_date,description" :
                    "name,category_id,max_budget,min_budget,expiry_date,description"
            );
            break;
        case RequestType.employer_edit:
            field_list = (
                parseInt(field_list_type) === FieldListType.all ?
                    "name,surname,freelancer_radio,email,location,current_password,website_link,twitter_link,facebook_link,linkedin_link,country_id,detail,new_password,confirm_password,tag_line,two_step,company_name,keyword_value" :
                    "name,surname,company_name,email,detail,location,tag_line,country_id"
            );
            break;
    }
    if (field_list === "*" && parseInt(field_list_type) === parseInt(FieldListType.required)) {
        field_list = deterReqFieldList(request_type, FieldListType.all);
    }
    return field_list;
}

function prepReqData(request_type) {
    var field_list = deterReqFieldList(request_type, FieldListType.all);
    var fields = field_list.split(',');
    var out_data = new Object();
    for (var field in fields) {
        field_name = fields[field];
        var input_type = $('#' + field_name).attr('type');
        if (input_type === "radio" || input_type === "checkbox") {
            out_data[field_name] = ($('#' + field_name).is(':checked') ? 1 : 2);
        } else {
            out_data[field_name] = $('#' + field_name).val();
        }
    }
    return out_data;
}

function clearFields(request_type) {
    var field_list = deterReqFieldList(request_type, FieldListType.all);
    switch (parseInt(request_type)) {
        case RequestType.register_user:
            field_list = field_list + "password_repeat";
            break;
    }
    var fields = field_list.split(',');
    var out_data = new Object();
    for (var field in fields) {
        field_name = fields[field];
        var input_type = $('#' + field_name).attr('type');
        if (input_type !== "radio" && input_type !== "checkbox") {
            $('#' + field_name).val("");
        }
    }
    return true;
}

function handleRequest(request_type, url) {
    if (!isReqFieldsValid(request_type) || !doOtherValidation(request_type)) {
        return false;
    }
    toggleLoader(true);
    var in_data = JSON.stringify(prepReqData(request_type), null, 2);
    var save_data = false;
    var return_data = null;
    save_data = $.ajax({
        type: g_post_method,
        url: url,
        data: in_data,
        dataType: 'json',
        contentType: 'application/json',
        success: function (result) {
            return_data = result;
            handleSuccessResponse(request_type, return_data);
        },
        complete: function () {
            toggleLoader(false);
        }
    });
    save_data.error(function (data) { handleErrorResponse(); });
}

function handleErrorResponse() {
    showErrorNotification(g_exception_message);
}

function handleSuccessResponse(request_type, return_data) {
    try {
        var _Obj = return_data;
        if (_Obj.ResponseStatus === ResponseStatus.success) {
            showSuccessNotification(_Obj.Description);
        } else {
            showErrorNotification(_Obj.Description);
        }
        doOtherResponseTask(request_type, _Obj.ResponseStatus);
        if (_Obj.JsFunc !== "") {
            $(function () {
                eval(_Obj.JsFunc);
            });
        }
    } catch (_Ex) {
        showErrorNotification(g_exception_message);
    }
    return true;
}

function doOtherResponseTask(request_type, response_status) {
    switch (parseInt(request_type)) {
        case RequestType.register_user:
            if (response_status === ResponseStatus.success) {
                $("#login_tab_link").click();
                clearFields(request_type);
            }
            break;
    }
}

function showSuccessNotification(notification_text) {
    Snackbar.show({
        text: notification_text,
        pos: 'top-center',
        showAction: false,
        actionText: "Dismiss",
        duration: 8000,
        textColor: '#000',
        backgroundColor: '#ccffcc',
        animation: "fadein 0.5s, fadeout 0.5s 2.5s"
        //customClass: "succ_mess"
    });
}

function showErrorNotification(notification_text) {
    Snackbar.show({
        text: notification_text,
        pos: 'top-center',
        showAction: false,
        actionText: "Dismiss",
        duration: 8000,
        textColor: '#000',
        backgroundColor: '#f9e0e0'
        //customClass: "err_mess"
    });
}

function showWarningNotification(notification_text) {
    Snackbar.show({
        text: notification_text,
        pos: 'top-center',
        showAction: false,
        actionText: "Dismiss",
        duration: 8000,
        textColor: '#000',
        backgroundColor: '#fee9d5'
        //customClass: "warn_mess"
    });
}

function doOtherValidation(request_type) {
    var return_val = true;
    var err_message = "";
    switch (parseInt(request_type)) {
        case RequestType.register_user:
            if ($("#password").val() !== $("#password_repeat").val()) {
                return_val = false;
                err_message = "The two passwords fields must contain the same text.";
                setFieldValidity("password_repeat", false);
                setFieldValidity("password", false);
            }
            break;
        case RequestType.freelancer_edit:
        case RequestType.employer_edit:
            if ($("#current_password").val() !== "" && ($("#new_password").val() === "" || $("#confirm_password").val() === "")) {
                return_val = false;
                err_message = "You should specify the New Password, since the Current Password field is not empty.";
                setFieldValidity("new_password", false);
                setFieldValidity("confirm_password", false);
            }
            if ($("#current_password").val() !== "" && $("#new_password").val() !== $("#confirm_password").val()) {
                return_val = false;
                err_message = "The two passwords fields must contain the same text.";
                setFieldValidity("new_password", false);
                setFieldValidity("confirm_password", false);
            }
            break;
    }
    if (!return_val) {
        showErrorNotification(err_message);
    }
    return return_val;
}

function isReqFieldsValid(request_type) {
    var check_fields = deterReqFieldList(request_type, FieldListType.required);
    if (check_fields === "") {
        return true;
    }
    var field_value = '',
        fields = check_fields.split(','),
        fields_least_one,
        return_value = true,
        all_is_empty = true,
        field_name = '',
        focus_field = '';
    for (var field in fields) {
        field_name = fields[field];
        if (field_name.indexOf(':') === -1) {
            field_value = document.getElementById(field_name).value;
            if (field_value === ''
                || field_value === '0') {
                if (!document.getElementById(field_name).disabled
                    && focus_field === '') {
                    focus_field = field_name;
                }
                setFieldValidity(field_name, false);
                if (return_value) {
                    return_value = false;
                }
            } else {
                setFieldValidity(field_name, true);
            }
        }
    }
    if (focus_field !== '') {
        $('#' + focus_field).focus();
    }
    if (!return_value) {
        showErrorNotification("The highlighted fields are required, and should be completed.");
    }
    return return_value;
}

function setFieldValidity(field_name, is_valid) {
    if (!is_valid) {
        $('#' + field_name).css("background-color", "#ffe6e6");
        bindValidatedField(field_name);
        $('#' + field_name).focus();
    } else {
        $('#' + field_name).css("background-color", "");
    }
}

function bindValidatedField(bind_fields) {
    var fields = bind_fields.split(',');
    for (var field in fields) {
        var field_name = fields[field];
        $('#' + field_name).bind('change', function () {
            if (this.value !== ''
                && this.value !== '0') {
                $('#' + this.id)
                    .css("background-color", "");
            } else {
                $('#' + this.id)
                    .css("background-color", "#ffe6e6");
            }
        });
    }
}

function updateMenuItemAsActive(menu_id) {
    if ($('#' + menu_id).length) {
        $('#' + menu_id).addClass("active");
    }
}

function initKeyWords(key_value) {
    if (key_value === "") {
        return true;
    }
    var curr_val = key_value.split(',');
    var item_name;
    for (var item in curr_val) {
        item_name = curr_val[item];
        var new_keyword = $("<span class='keyword'><span class='keyword-remove'></span><span class='keyword-text'>" + item_name + "</span></span>");
        $(".keywords-list").append(new_keyword).trigger('resizeContainer');
    }
}