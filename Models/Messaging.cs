﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

public class Messaging {
    #region Member Variables
    protected string _sUuid;
    protected string _sText;
    protected string _sLinkUuid;
    protected MessageStatus _MessageStatus;
    protected string _sSenderUuid;
    protected string _sRecipientUuid;
    private DateTime _dtCreated;
    #endregion

    #region Constructors
    public Messaging() {
    }

    public Messaging( string _sUuid, string _sText, string _sLinkUuid, MessageStatus _MessageStatus, string _sSenderUuid, string _sRecipientUuid, DateTime _dtCreated ) {
        this._sUuid = _sUuid;
        this._sText = _sText;
        this._sLinkUuid = _sLinkUuid;
        this._MessageStatus = _MessageStatus;
        this._sSenderUuid = _sSenderUuid;
        this._sRecipientUuid = _sRecipientUuid;
        this._dtCreated = _dtCreated;
    }
    #endregion
    #region Public Properties
    public string Uuid {
        get { return _sUuid; }
        set { _sUuid = value; }
    }
    public string Text {
        get { return _sText; }
        set { _sText = value; }
    }
    public string Link_uuid {
        get { return _sLinkUuid; }
        set { _sLinkUuid = value; }
    }
    public MessageStatus MessageStatus {
        get { return _MessageStatus; }
        set { _MessageStatus = value; }
    }
    public string SenderUuid {
        get { return _sSenderUuid; }
        set { _sSenderUuid = value; }
    }
    public string RecipientUuid {
        get { return _sRecipientUuid; }
        set { _sRecipientUuid = value; }
    }
    public DateTime Created {
        get { return _dtCreated; }
        set { _dtCreated = value; }
    }
    public static string g_sTableName = SqlTable.tbl_message.ToString();
    #endregion

    #region Public Properties
    public bool set() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += ( _bExist ? "update " : "insert into " ) + g_sTableName;
            _sQuery += " set text = '" + Conv.clean( this.Text ) + "',";
            _sQuery += " link_uuid = '" + Conv.clean( this.Link_uuid ) + "',";
            _sQuery += " enum_message_status_id = '" + ( int )this.MessageStatus + "',";
            _sQuery += " sender_uuid = '" + Conv.clean( this.SenderUuid ) + "',";
            _sQuery += " recipient_uuid = '" + Conv.clean( this.RecipientUuid ) + "',";
            _sQuery += ( !_bExist ? ", created = now()" : "" );
            _sQuery += ( !_bExist ? ", uuid = " : " where uuid = " ) + "'" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Message", "set", _Ex.Message );
        }
        return false;
    }

    public bool delete() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += "update " + g_sTableName;
            _sQuery += " set soft_del = now() where uuid = '" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Message", "delete", _Ex.Message );
        }
        return false;
    }

    public bool markAsRead() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += "update " + g_sTableName;
            _sQuery += " set date_read = now() where uuid = '" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Message", "markAsRead", _Ex.Message );
        }
        return false;
    }

    public bool exist( string _sUuid ) {
        try {
            return SqlClass.numRow( "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'" ) > 0;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Message", "exist", _Ex.Message );
        }
        return false;
    }

    public static Messaging getObj( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            DataRow _Dr = SqlClass.getDataRow( _sQuery );
            if ( _Dr == null ) {
                return null;
            }
            return new Messaging(
                _Dr["uuid"].ToString(),
                _Dr["text"].ToString(),
                _Dr["link_uuid"].ToString(),
                ( MessageStatus )int.Parse( _Dr["enum_message_status_id"].ToString() ),
                _Dr["sender_uuid"].ToString(),
                _Dr["recipient_uuid"].ToString(),
                 DateTime.Parse( _Dr["created"].ToString() )
                );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Message", "getObj", _Ex.Message );
        }
        return null;
    }

    public static dynamic getRow( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            return SqlClass.getDataRow( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Message", "getRow", _Ex.Message );
        }
        return null;
    }

    public static string deterQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
        try {
            _sSearchText = Conv.clean( _sSearchText.Trim() );
            string _sQuery = @"select 
                                            u.uuid,
                                            u.username,
                                            u.first_name,
                                            u.last_name,
                                            u.email,
                                            u.cell_no,
                                            ifnull(concat(p.last_name, ' ', p.first_name),'') as pilot,
                                            s.name as status
                                        from tbl_user u
                                        inner join lu_u_status s on s.id = u.enum_status_id
                                        left outer join tbl_p_pilot p on p.uuid= u.pilot_uuid
                                        where 1=1";
            _sQuery += ( _sUuid == "" || _sUuid == "0" ? "" : " and ac.uuid = '" + _sUuid + "'" );
            _sQuery += ( _sStatus.Trim() == "" || _sStatus == "0" ? "" : " and u.enum_status_id = '" + _sStatus + "'" );
            _sQuery += ( _sIsPilot.Trim() == "" || _sIsPilot == "0" ? "" : " and u.is_pilot = '" + _sIsPilot + "'" );
            _sQuery += ( _sSearchText == "" ? "" : " and (u.username like '%" + _sSearchText + "%' or  u.first_name like '%" + _sSearchText + @"%' or u.last_name like '%" + _sSearchText + @"%'
                                                         or u.email like '%" + _sSearchText + "%' or p.first_name like '" + _sSearchText + @"'
                                                          or p.last_name like '%" + _sSearchText + "%')" );
            _sQuery += " order by s.name, u.last_name, u.first_name;";
            return _sQuery;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Message", "deterQuery", _Ex.Message );
        }
        return String.Empty;
    }

    //public static IEnumerable<dynamic> getFromQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
    //    try {
    //        return SqlClass.getIEnumerableList( deterQuery( _sUuid, _sStatus, _sIsPilot, _sSearchText ) );
    //    } catch ( Exception _Ex ) {
    //        ErrorHandler.handleError( "Class", "Message", "deterQuery", _Ex.Message );
    //    }
    //    return null;
    //}
    #endregion
}