﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

public class Project {
    #region Member Variables
    protected string _sUuid;
    protected string _sTitle;
    protected int _iJobCategory;
    protected PaymentBasis _PaymentBasis;
    protected string _sLocation;
    protected decimal _dMinBudget;
    protected decimal _dMaxBudget;
    protected string _sDescription;
    protected string _sTags;
    protected string _sUserUuid;
    protected DateTime _dtExpiryDate;
    #endregion

    #region Constructors
    public Project() {
    }

    public Project( string _sUuid, string _sTitle, int _iJobCategory, PaymentBasis _PaymentBasis, string _sLocation,
        decimal _dMinBudget, decimal _dMaxBudget, string _sDescription, string _sTags, string _sUserUuid, DateTime _dtExpiryDate ) {
        this._sUuid = _sUuid;
        this._sTitle = _sTitle;
        this._iJobCategory = _iJobCategory;
        this._PaymentBasis = _PaymentBasis;
        this._sLocation = _sLocation;
        this._dMinBudget = _dMinBudget;
        this._dMaxBudget = _dMaxBudget;
        this._sDescription = _sDescription;
        this._sTags = _sTags;
        this._sUserUuid = _sUserUuid;
        this._dtExpiryDate = _dtExpiryDate;
    }
    #endregion

    #region Public Properties
    public string Uuid {
        get { return _sUuid; }
        set { _sUuid = value; }
    }
    public string Title {
        get { return _sTitle; }
        set { _sTitle = value; }
    }
    public int JobCategory {
        get { return _iJobCategory; }
        set { _iJobCategory = value; }
    }
    public PaymentBasis PaymentBasis {
        get { return _PaymentBasis; }
        set { _PaymentBasis = value; }
    }
    public string Location {
        get { return _sLocation; }
        set { _sLocation = value; }
    }
    public decimal MinBudget {
        get { return _dMinBudget; }
        set { _dMinBudget = value; }
    }
    public decimal MaxBudget {
        get { return _dMaxBudget; }
        set { _dMaxBudget = value; }
    }
    public string Description {
        get { return _sDescription; }
        set { _sDescription = value; }
    }
    public string Tags {
        get { return _sTags; }
        set { _sTags = value; }
    }
    public string UserUuid {
        get { return _sUserUuid; }
        set { _sUserUuid = value; }
    }
    public DateTime ExpiryDate {
        get { return _dtExpiryDate; }
        set { _dtExpiryDate = value; }
    }
    public static string g_sTableName = SqlTable.tbl_project.ToString();
    #endregion

    #region Public Properties
    public bool set() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += ( _bExist ? "update " : "insert into " ) + g_sTableName;
            _sQuery += " set title = '" + Conv.clean( this.Title ) + "',";
            _sQuery += " enum_category_id = '" + this.JobCategory + "',";
            _sQuery += " enum_payment_basis_id = '" + (int)this.PaymentBasis + "',";
            _sQuery += " min_budget = '" + this.MinBudget + "',";
            _sQuery += " max_budget = '" + this.MaxBudget + "',";
            _sQuery += " description = '" + Conv.clean( this.Description ) + "',";
            _sQuery += " tags = '" + Conv.clean( this.Tags ) + "',";
            _sQuery += " user_uuid = '" + Conv.clean( this.UserUuid ) + "',";
            _sQuery += " expiry_date = '" + this.ExpiryDate.ToString( Constant.SQL_DATE_FORMAT_LONG ) + "',";
            _sQuery += "last_modified = now()";
            _sQuery += ( !_bExist ? ", created = now()" : "" );
            _sQuery += ( !_bExist ? ", uuid = " : " where uuid = " ) + "'" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Project", "set", _Ex.Message );
        }
        return false;
    }

    public bool delete() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += "update " + g_sTableName;
            _sQuery += " set soft_del = now() where uuid = '" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Project", "unset", _Ex.Message );
        }
        return false;
    }

    public bool exist( string _sUuid ) {
        try {
            return SqlClass.numRow( "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'" ) > 0;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Project", "exist", _Ex.Message );
        }
        return false;
    }

    public static Project getObj( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            DataRow _Dr = SqlClass.getDataRow( _sQuery );
            if ( _Dr == null ) {
                return null;
            }
            return new Project(
                _Dr["uuid"].ToString(),
                _Dr["title"].ToString(),
                int.Parse( _Dr["enum_category_id"].ToString() ),
                (PaymentBasis)int.Parse( _Dr["enum_payment_basis_id"].ToString() ),
                _Dr["location"].ToString(),
                decimal.Parse( _Dr["min_budget"].ToString() ),
                decimal.Parse( _Dr["max_budget"].ToString() ),
                _Dr["description"].ToString(),
                _Dr["tags"].ToString(),
                _Dr["user_uuid"].ToString(),
                DateTime.Parse( _Dr["expiry_date"].ToString() )
                );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Project", "get", _Ex.Message );
        }
        return null;
    }

    public static dynamic getRow( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            return SqlClass.getDataRow( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Project", "get", _Ex.Message );
        }
        return null;
    }

    public static string deterQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
        try {
            _sSearchText = Conv.clean( _sSearchText.Trim() );
            string _sQuery = @"select 
                                            u.uuid,
                                            u.username,
                                            u.first_name,
                                            u.last_name,
                                            u.email,
                                            u.cell_no,
                                            ifnull(concat(p.last_name, ' ', p.first_name),'') as pilot,
                                            s.name as status
                                        from tbl_user u
                                        inner join lu_u_status s on s.id = u.enum_status_id
                                        left outer join tbl_p_pilot p on p.uuid= u.pilot_uuid
                                        where 1=1";
            _sQuery += ( _sUuid == "" || _sUuid == "0" ? "" : " and ac.uuid = '" + _sUuid + "'" );
            _sQuery += ( _sStatus.Trim() == "" || _sStatus == "0" ? "" : " and u.enum_status_id = '" + _sStatus + "'" );
            _sQuery += ( _sIsPilot.Trim() == "" || _sIsPilot == "0" ? "" : " and u.is_pilot = '" + _sIsPilot + "'" );
            _sQuery += ( _sSearchText == "" ? "" : " and (u.username like '%" + _sSearchText + "%' or  u.first_name like '%" + _sSearchText + @"%' or u.last_name like '%" + _sSearchText + @"%'
                                                         or u.email like '%" + _sSearchText + "%' or p.first_name like '" + _sSearchText + @"'
                                                          or p.last_name like '%" + _sSearchText + "%')" );
            _sQuery += " order by s.name, u.last_name, u.first_name;";
            return _sQuery;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Project", "deterQuery", _Ex.Message );
        }
        return String.Empty;
    }

    //public static IEnumerable<dynamic> getFromQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
    //    try {
    //        return SqlClass.getIEnumerableList( deterQuery( _sUuid, _sStatus, _sIsPilot, _sSearchText ) );
    //    } catch ( Exception _Ex ) {
    //        ErrorHandler.handleError( "Class", "Project", "deterQuery", _Ex.Message );
    //    }
    //    return null;
    //}
    #endregion
}