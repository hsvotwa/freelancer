﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Freelancer.Models {
    public class FreelancerEmp {
        #region Member Variables
        protected string _sUuid;
        protected string _sUserUuid;
        protected decimal _dMinHourlyRate;
        protected string _sSkillTags;
        protected string _sTagLine;
        protected string _sBio;
        protected Status _Status;
        protected Gender _Gender;
        protected int _iYearsExperience;
        protected bool _bEmployed;
        public static FreelancerEmp CurrentUser = null;
        #endregion

        #region Constructors
        public FreelancerEmp() {
        }

        public FreelancerEmp( string uuid, string _sUserUuid, decimal _dMinHourlyRate,
            string _sSkillTags, string _sTagLine, string _sBio, Status _Status, Gender _Gender, int _iYearsExperience, bool _bEmployed ) {
            this._sUuid = uuid;
            this._sUserUuid = _sUserUuid;
            this._dMinHourlyRate = _dMinHourlyRate;
            this._sSkillTags = _sSkillTags;
            this._sTagLine = _sTagLine;
            this._sBio = _sBio;
            this._Status = _Status;
            this._Gender = _Gender;
            this._iYearsExperience = _iYearsExperience;
            this._bEmployed = _bEmployed;
        }
        #endregion
        #region Public Properties
        public string Uuid {
            get { return _sUuid; }
            set { _sUuid = value; }
        }
        public string UserUuid {
            get { return _sUserUuid; }
            set { _sUserUuid = value; }
        }
        public decimal MinHourlyRate {
            get { return _dMinHourlyRate; }
            set { _dMinHourlyRate = value; }
        }
        public string SkillTags {
            get { return _sSkillTags; }
            set { _sSkillTags = value; }
        }
        public string TagLine {
            get { return _sTagLine; }
            set { _sTagLine = value; }
        }
        public string Bio {
            get { return _sBio; }
            set { _sBio = value; }
        }
        public Status Status {
            get { return _Status; }
            set { _Status = value; }
        }
        public Gender Gender {
            get { return _Gender; }
            set { _Gender = value; }
        }
        public int YearsExperience {
            get { return _iYearsExperience; }
            set { _iYearsExperience = value; }
        }
        public bool Employed {
            get { return _bEmployed; }
            set { _bEmployed = value; }
        }

        public static string g_sTableName = SqlTable.tbl_freelancer.ToString();
        #endregion

        #region Public Properties
        public bool set() {
            try {
                bool _bExist = this.exist( this.Uuid );
                string _sQuery = String.Empty;
                _sQuery += ( _bExist ? "update " : "insert into " ) + g_sTableName;
                _sQuery += " set min_hourly_rate = '" + this.MinHourlyRate + "',";
                _sQuery += "user_uuid = '" + this.UserUuid + "',";
                _sQuery += "skill_tags = '" + this.SkillTags + "',";
                _sQuery += "tag_line = '" + this.TagLine + "',";
                _sQuery += "bio = '" + this.Bio + "',";
                _sQuery += "enum_gender_id = '" + (int)this.Gender + "',";
                _sQuery += "years_experience = '" + this.YearsExperience + "',";
                _sQuery += "employed = '" + ( this.Employed ? (int)YesNo.yes : (int)YesNo.no ) + "',";
                _sQuery += "enum_online_status_id = '" + (int)this.Status + "',";
                //_sQuery += ( !_bExist ? "force_password_change = 1," : "" );
                _sQuery += "last_modified = now()";
                _sQuery += ( !_bExist ? ", created = now()" : "" );
                _sQuery += ( !_bExist ? ", uuid = " : " where uuid = " ) + "'" + this.Uuid + "'";
                return SqlClass.execCmdText( _sQuery );
            } catch ( Exception _Ex ) {
                ErrorHandler.handleError( "Class", "Freelancer", "set", _Ex.Message );
            }
            return false;
        }

        public bool unset() {
            try {
                bool _bExist = this.exist( this.Uuid );
                string _sQuery = String.Empty;
                _sQuery += "update " + g_sTableName;
                _sQuery += " set soft_del = now() where uuid = '" + this.Uuid + "'";
                return SqlClass.execCmdText( _sQuery );
            } catch ( Exception _Ex ) {
                ErrorHandler.handleError( "Class", "Freelancer", "unset", _Ex.Message );
            }
            return false;
        }

        public bool exist( string _sUuid ) {
            try {
                return SqlClass.numRow( "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'" ) > 0;
            } catch ( Exception _Ex ) {
                ErrorHandler.handleError( "Class", "Freelancer", "exist", _Ex.Message );
            }
            return false;
        }

        public static FreelancerEmp getObj( string _sKey ) {
            try {
                string _sQuery = "select * from " + g_sTableName + " where user_uuid = '" + _sKey + "'";
                DataRow _Dr = SqlClass.getDataRow( _sQuery );
                if ( _Dr == null ) {
                    return null;
                }
                return new FreelancerEmp(
                    _Dr["uuid"].ToString(),
                    _Dr["user_uuid"].ToString(),
                    decimal.Parse( _Dr["min_hourly_rate"].ToString() ),
                    _Dr["skill_tags"].ToString(),
                    _Dr["tag_line"].ToString(),
                    _Dr["bio"].ToString(),
                    (Status)int.Parse( _Dr["enum_status_id"].ToString() ),
                   (Gender)int.Parse( _Dr["enum_gender_id"].ToString() ),
                    int.Parse( _Dr["years_experience"].ToString() ),
                    int.Parse( _Dr["employed"].ToString() ) == (int)YesNo.yes
                    );
            } catch ( Exception _Ex ) {
                ErrorHandler.handleError( "Class", "Freelancer", "getObj", _Ex.Message );
            }
            return null;
        }

        public DataRow getDataRow() {
            try {
                string _sQuery = @"select 
                                                f.*, 
                                                u.name ,
                                                u.surname,
                                                u.email,
                                                u.enum_account_type_id,
                                                u.2_step_verification,
                                                u.enum_status_id
                                            from tbl_freelancer f
                                            left join tbl_user u on u.uuid = f.user_uuid
                                            where f.uuid = '" + this.Uuid + "'";
                return SqlClass.getDataRow( _sQuery );
            } catch ( Exception _Ex ) {
                ErrorHandler.handleError( "Class", "Freelancer", "getDynamicRow", _Ex.Message );
            }
            return null;
        }

        public static bool exists( string _sGuid ) {
            try {
                string _sQuery = @"select * from tbl_freelancer where uuid= '" + _sGuid + "'";
                return SqlClass.numRow( _sQuery ) > 0;
            } catch ( Exception _Ex ) {
                ErrorHandler.handleError( "Class", "Freelancer", "exists", _Ex.Message );
            }
            return false;
        }
        #endregion
    }
}