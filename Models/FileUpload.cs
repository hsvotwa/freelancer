﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;

public class FileUpload {
    #region Member Variables
    protected string _sUuid;
    protected string _sLinkUuid;
    protected string _sServerName;
    protected string _sGivenName;
    protected FileType _FileType;
    protected Status _Status;
    protected string _sUserUuid;
    #endregion

    #region Constructors
    public FileUpload() {
    }
    public FileUpload( string _sUuid, string _sLinkUuid, string _sServerName, string _sGivenName, FileType _FileType, Status _Status, string _sUserUuid ) {
        this._sUuid = _sUuid;
        this._sLinkUuid = _sLinkUuid;
        this._sServerName = _sServerName;
        this._sGivenName = _sGivenName;
        this._FileType = _FileType;
        this._Status = _Status;
        this._sUserUuid = _sUserUuid;
    }
    #endregion
    #region Public Properties
    public string LinkUuid {
        get { return _sLinkUuid; }
        set { _sLinkUuid = value; }
    }
    public virtual string ServerName {
        get { return _sServerName; }
        set { _sServerName = value; }
    }
    public virtual string GivenName {
        get { return _sGivenName; }
        set { _sGivenName = value; }
    }
    public FileType FileType {
        get { return _FileType; }
        set { _FileType = value; }
    }
    public Status Status {
        get { return _Status; }
        set { _Status = value; }
    }
    public virtual string UserUuid {
        get { return _sUserUuid; }
        set { _sUserUuid = value; }
    }

    public string Uuid { get => this._sUuid; set => this._sUuid = value; }

    public static string g_sTableName = SqlTable.tbl_file.ToString();
    #endregion

    #region Public Properties
    public bool set() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += ( _bExist ? "update " : "insert into " ) + g_sTableName;
            _sQuery += " set link_uuid = '" + Conv.clean( this.LinkUuid ) + "',";
            _sQuery += "server_name = '" + Conv.clean( this.ServerName ) + "',";
            _sQuery += "given_name = '" + Conv.clean( this.GivenName ) + "',";
            _sQuery += "enum_type_id = '" + ( int )this.FileType + "',";
            _sQuery += "enum_status_id = '" + ( int )this.Status + "',";
            _sQuery += "user_uuid = '" + Conv.clean( this.UserUuid ) + "',";
            _sQuery += "last_modified = now()";
            _sQuery += ( !_bExist ? ", created = now()" : "" );
            _sQuery += ( !_bExist ? ", uuid = " : " where uuid = " ) + "'" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "File", "set", _Ex.Message );
        }
        return false;
    }

    public bool delete() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += "update " + g_sTableName;
            _sQuery += " set enum_status_id = '" + Status.deleted + "' where uuid = '" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "File", "unset", _Ex.Message );
        }
        return false;
    }

    public bool exist( string _sUuid ) {
        try {
            return SqlClass.numRow( "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'" ) > 0;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "File", "exist", _Ex.Message );
        }
        return false;
    }

    public static FileUpload getObj( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            DataRow _Dr = SqlClass.getDataRow( _sQuery );
            if ( _Dr == null ) {
                return null;
            }
            return new FileUpload(
                    _Dr["uuid"].ToString(),
                    _Dr["link_uuid"].ToString(),
                     _Dr["server_name"].ToString(),
                      _Dr["given_name"].ToString(),
                       ( FileType )int.Parse( _Dr["enum_type_id"].ToString() ),
                       ( Status )int.Parse( _Dr["link_uuid"].ToString() ),
                    _Dr["user_uuid"].ToString()
                    );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "File", "get", _Ex.Message );
        }
        return null;
    }

    public static dynamic getRow( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            return SqlClass.getDataRow( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "File", "get", _Ex.Message );
        }
        return null;
    }

    public static DataSet getAllLinkedFile( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where link_uuid = '" + _sUuid + "' order by created desc limit 6";
            return SqlClass.getDataSet( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "File", "getAllLinkedFile", _Ex.Message );
        }
        return null;
    }

    public static string deterQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
        try {
            _sSearchText = Conv.clean( _sSearchText.Trim() );
            string _sQuery = @"select 
                                            u.uuid,
                                            u.username,
                                            u.first_name,
                                            u.last_name,
                                            u.email,
                                            u.cell_no,
                                            ifnull(concat(p.last_name, ' ', p.first_name),'') as pilot,
                                            s.name as status
                                        from tbl_user u
                                        inner join lu_u_status s on s.id = u.enum_status_id
                                        left outer join tbl_p_pilot p on p.uuid= u.pilot_uuid
                                        where 1=1";
            _sQuery += ( _sUuid == "" || _sUuid == "0" ? "" : " and ac.uuid = '" + _sUuid + "'" );
            _sQuery += ( _sStatus.Trim() == "" || _sStatus == "0" ? "" : " and u.enum_status_id = '" + _sStatus + "'" );
            _sQuery += ( _sIsPilot.Trim() == "" || _sIsPilot == "0" ? "" : " and u.is_pilot = '" + _sIsPilot + "'" );
            _sQuery += ( _sSearchText == "" ? "" : " and (u.username like '%" + _sSearchText + "%' or  u.first_name like '%" + _sSearchText + @"%' or u.last_name like '%" + _sSearchText + @"%'
                                                         or u.email like '%" + _sSearchText + "%' or p.first_name like '" + _sSearchText + @"'
                                                          or p.last_name like '%" + _sSearchText + "%')" );
            _sQuery += " order by s.name, u.last_name, u.first_name;";
            return _sQuery;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "File", "deterQuery", _Ex.Message );
        }
        return String.Empty;
    }

    //public static IEnumerable<dynamic> getFromQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
    //    try {
    //        return SqlClass.getIEnumerableList( deterQuery( _sUuid, _sStatus, _sIsPilot, _sSearchText ) );
    //    } catch ( Exception _Ex ) {
    //        ErrorHandler.handleError( "Class", "File", "deterQuery", _Ex.Message );
    //    }
    //    return null;
    //}
    #endregion
}