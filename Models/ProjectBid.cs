﻿using System;
using System.Collections.Generic;
using System.Data;

public class ProjectBid {
    #region Member Variables
    protected string _sUuid;
    protected decimal _dBudget;
    protected decimal _dDeliveryTime;
    protected TimeUnit _TimeUnit;
    protected string _sUserUuid;
    protected string _sProjectUuid;
    protected Status _Status;
    #endregion

    #region Constructors
    public ProjectBid() {
    }

    public ProjectBid( string _sUuid, decimal _dBudget, decimal _dDeliveryTime, TimeUnit _TimeUnit, string _sUserUuid, string _sProjectUuid, Status _Status ) {
        this._sUuid = _sUuid;
        this._dBudget = _dBudget;
        this._dDeliveryTime = _dDeliveryTime;
        this._TimeUnit = _TimeUnit;
        this._sUserUuid = _sUserUuid;
        this._sProjectUuid = _sProjectUuid;
        this._Status = _Status;
    }
    #endregion

    #region Public Properties
    public string Uuid {
        get { return _sUuid; }
        set { _sUuid = value; }
    }
    public decimal Budget {
        get { return _dBudget; }
        set { _dBudget = value; }
    }
    public decimal DeliveryTime {
        get { return _dDeliveryTime; }
        set { _dDeliveryTime = value; }
    }
    public TimeUnit TimeUnit {
        get { return _TimeUnit; }
        set { _TimeUnit = value; }
    }
    public string UserUuid {
        get { return _sUserUuid; }
        set { _sUserUuid = value; }
    }
    public string ProjectUuid {
        get { return _sProjectUuid; }
        set { _sProjectUuid = value; }
    }
    public Status Status {
        get { return _Status; }
        set { _Status = value; }
    }
    public static string g_sTableName = SqlTable.tbl_project_bid.ToString();
    #endregion

    #region Public Properties
    public bool set() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += ( _bExist ? "update " : "insert into " ) + g_sTableName;
            _sQuery += " set project_uuid = '" + Conv.clean( this.ProjectUuid ) + "',";
            _sQuery += " user_uuid = '" + Conv.clean( this.UserUuid ) + "',";
            _sQuery += " budget = '" + this.Budget + "',";
            _sQuery += " delivery_time = '" + this.DeliveryTime + "',";
            _sQuery += " enum_time_unit_id = '" + ( int )this.TimeUnit + "',";
            _sQuery += " enum_status_id = '" + ( int )this.Status + "',";
            _sQuery += ( !_bExist ? ", created = now()" : "" );
            _sQuery += ( !_bExist ? ", uuid = " : " where uuid = " ) + "'" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "ProjectBid", "set", _Ex.Message );
        }
        return false;
    }

    public bool updStatus( Status _NewStatus ) {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += "update " + g_sTableName;
            _sQuery += " set enum_status_id =  '" + ( int )_NewStatus + "' where uuid = '" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "ProjectBid", "updStatus", _Ex.Message );
        }
        return false;
    }

    public bool delete() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += "update " + g_sTableName;
            _sQuery += " set soft_del = now() where uuid = '" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Review", "delete", _Ex.Message );
        }
        return false;
    }

    public bool exist( string _sUuid ) {
        try {
            return SqlClass.numRow( "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'" ) > 0;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "ProjectBid", "exist", _Ex.Message );
        }
        return false;
    }

    public static ProjectBid getObj( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            DataRow _Dr = SqlClass.getDataRow( _sQuery );
            if ( _Dr == null ) {
                return null;
            }
            return new ProjectBid(
                _Dr["uuid"].ToString(),
                decimal.Parse( _Dr["budget"].ToString() ),
                decimal.Parse( _Dr["delivery_time"].ToString() ),
                ( TimeUnit )int.Parse( _Dr["enum_time_unit_id"].ToString() ),
                _Dr["user_uuid"].ToString(),
                _Dr["project_uuid"].ToString(),
                ( Status )int.Parse( _Dr["enum_status_id"].ToString() )
                );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "ProjectBid", "get", _Ex.Message );
        }
        return null;
    }

    public static dynamic getRow( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            return SqlClass.getDataRow( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "ProjectBid", "get", _Ex.Message );
        }
        return null;
    }

    public static string deterQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
        try {
            _sSearchText = Conv.clean( _sSearchText.Trim() );
            string _sQuery = @"select 
                                            u.uuid,
                                            u.username,
                                            u.first_name,
                                            u.last_name,
                                            u.email,
                                            u.cell_no,
                                            ifnull(concat(p.last_name, ' ', p.first_name),'') as pilot,
                                            s.name as status
                                        from tbl_user u
                                        inner join lu_u_status s on s.id = u.enum_status_id
                                        left outer join tbl_p_pilot p on p.uuid= u.pilot_uuid
                                        where 1=1";
            _sQuery += ( _sUuid == "" || _sUuid == "0" ? "" : " and ac.uuid = '" + _sUuid + "'" );
            _sQuery += ( _sStatus.Trim() == "" || _sStatus == "0" ? "" : " and u.enum_status_id = '" + _sStatus + "'" );
            _sQuery += ( _sIsPilot.Trim() == "" || _sIsPilot == "0" ? "" : " and u.is_pilot = '" + _sIsPilot + "'" );
            _sQuery += ( _sSearchText == "" ? "" : " and (u.username like '%" + _sSearchText + "%' or  u.first_name like '%" + _sSearchText + @"%' or u.last_name like '%" + _sSearchText + @"%'
                                                         or u.email like '%" + _sSearchText + "%' or p.first_name like '" + _sSearchText + @"'
                                                          or p.last_name like '%" + _sSearchText + "%')" );
            _sQuery += " order by s.name, u.last_name, u.first_name;";
            return _sQuery;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "ProjectBid", "deterQuery", _Ex.Message );
        }
        return String.Empty;
    }

    //public static IEnumerable<dynamic> getFromQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
    //    try {
    //        return SqlClass.getIEnumerableList( deterQuery( _sUuid, _sStatus, _sIsPilot, _sSearchText ) );
    //    } catch ( Exception _Ex ) {
    //        ErrorHandler.handleError( "Class", "ProjectBid", "deterQuery", _Ex.Message );
    //    }
    //    return null;
    //}
    #endregion
}