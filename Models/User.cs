﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Text;

public class UserClass {
    #region Member Variables
    protected string _sUuid;
    protected string _sCompanyUuid;
    protected string _sName;
    protected string _sSurname;
    protected string _sEmail;
    protected AccountType _AccountType;
    protected string _sPassword;
    protected bool _bTwoStepVerification;
    protected Status _Status;
    public static UserClass CurrentUser = null;
    #endregion

    #region Constructors
    public UserClass() {
    }

    public UserClass( string uuid, string _sCompanyUuid, string _sName, string _sSurname, string _sEmail, AccountType _AccountType,
       string _sPassword, bool _bTwoStepVerification, Status _Status ) {
        this._sUuid = uuid;
        this._sCompanyUuid = _sCompanyUuid;
        this._sName = _sName;
        this._sSurname = _sSurname;
        this._sEmail = _sEmail;
        this._AccountType = _AccountType;
        this._sPassword = _sPassword;
        this._bTwoStepVerification = _bTwoStepVerification;
        this._Status = _Status;
    }
    #endregion
    #region Public Properties
    public string Uuid {
        get { return _sUuid; }
        set { _sUuid = value; }
    }
    public string CompanyUuid {
        get { return _sCompanyUuid; }
        set { _sCompanyUuid = value; }
    }
    public string Name {
        get { return _sName; }
        set { _sName = value; }
    }
    public string Surname {
        get { return _sSurname; }
        set { _sSurname = value; }
    }
    public string Email {
        get { return _sEmail; }
        set { _sEmail = value; }
    }
    public AccountType AccountType {
        get { return _AccountType; }
        set { _AccountType = value; }
    }
    public string Password {
        get { return _sPassword; }
        set { _sPassword = value; }
    }
    public bool TwoStepVerification {
        get { return _bTwoStepVerification; }
        set { _bTwoStepVerification = value; }
    }
    public Status Status {
        get { return _Status; }
        set { _Status = value; }
    }
    public static string g_sTableName = SqlTable.tbl_user.ToString();
    #endregion

    #region Public Properties
    public bool set(bool _bUpdPassword = false) {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += ( _bExist ? "update " : "insert into " ) + g_sTableName;
            _sQuery += " set name = '" + Conv.clean( this.Name ) + "',";
            _sQuery += "surname = '" + Conv.clean( this.Surname ) + "',";
            _sQuery += "email = '" + Conv.clean( this.Email ) + "',";
            _sQuery += "enum_account_type_id = '" + ( int )this.AccountType + "',";
            _sQuery += "2_step_verification = '" + ( this.TwoStepVerification ? ( int )YesNo.yes : ( int )YesNo.no ) + "',";
            _sQuery += this.CompanyUuid == "" ? "" : "company_uuid = '" + this.CompanyUuid + "',";
            _sQuery += ( !_bExist || _bUpdPassword ? "password = '" + Conv.clean( encryptPwd( this.Password ) ) + "'," : "" );
            _sQuery += "enum_status_id = '" + ( int )this.Status + "',";
            _sQuery += "last_modified = now()";
            _sQuery += ( !_bExist ? ", created = now()" : "" );
            _sQuery += ( !_bExist ? ", uuid = " : " where uuid = " ) + "'" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "UserClass", "set", _Ex.Message );
        }
        return false;
    }

    public bool unset() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += "update " + g_sTableName;
            _sQuery += " set soft_del = now() where uuid = '" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Bookmark", "unset", _Ex.Message );
        }
        return false;
    }

    public bool exist( string _sUuid ) {
        try {
            return SqlClass.numRow( "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'" ) > 0;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "UserClass", "exist", _Ex.Message );
        }
        return false;
    }

    public static string getProfPic( string _sUuid ) {
        string _sPath = "/images/user-avatar-placeholder.png";
        try {
            string _sQuery = "select server_name from tbl_file where link_uuid = '" + _sUuid + "' and enum_type_id= '" + (int)FileType.user_prof_pic + "' order by created desc limit 1";
            DataRow _Dr = SqlClass.getDataRow( _sQuery );
            if ( _Dr != null ) {
                _sPath = "/" + Constant.UPLOADS_FOLDER + "/" + _Dr["server_name"];
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "File", "get", _Ex.Message );
        }
        return _sPath;
    }

    public static UserClass getObj( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            DataRow _Dr = SqlClass.getDataRow( _sQuery );
            if ( _Dr == null ) {
                return null;
            }
            return new UserClass(
                _Dr["uuid"].ToString(),
                _Dr["company_uuid"].ToString(),
                _Dr["name"].ToString(),
                _Dr["surname"].ToString(),
                _Dr["email"].ToString(),
               ( AccountType )int.Parse( _Dr["enum_account_type_id"].ToString() ),
                _Dr["password"].ToString(),
                int.Parse( _Dr["2_step_verification"].ToString() ) == ( int )YesNo.yes,
                ( Status )int.Parse( _Dr["enum_status_id"].ToString() )
                );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "UserClass", "get", _Ex.Message );
        }
        return null;
    }

    public static dynamic getRow( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            return SqlClass.getDataRow( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Bookmark", "get", _Ex.Message );
        }
        return null;
    }

    public static UserClass getCurrentUser() {
        try {
            return UserClass.CurrentUser;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "UserClass", "getCurrentUser", _Ex.Message );
        }
        return null;
    }

    public bool authenticateUser( string _sUsername, string _sPassword, bool _bKeepSession = false ) {
        try {
            string _sQuery = "select uuid from tbl_user where email='" + _sUsername + "' and password='" + this.encryptPwd( _sPassword ) + "'";
            DataRow _Dr = SqlClass.getDataRow( _sQuery );
            if ( _Dr == null ) {
                return false;
            }
            string _sUserUuid = _Dr["uuid"].ToString();
            bool auth = ( !string.IsNullOrEmpty(_sUserUuid));
            if ( auth ) {
                UserSession.setUser( _sUserUuid, _bKeepSession );
                return true;
            }
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "UserClass", "authenticateUser", _Ex.Message );
        }
        return false;
    }

    public bool validPassword( string _sUsername, string _sPassword ) {
        try {
            string _sQuery = "select uuid from tbl_user where email='" + _sUsername + "' and password='" + this.encryptPwd( _sPassword ) + "'";
            DataRow _Dr = SqlClass.getDataRow( _sQuery );
            if ( _Dr == null ) {
                return false;
            }
            string _sUserUuid = _Dr["uuid"].ToString();
            return ( !string.IsNullOrEmpty( _sUserUuid ) );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "UserClass", "validPassword", _Ex.Message );
        }
        return false;
    }

    public string encryptPwd( string clearText, string _sEncryptionKey = "Zvibaba88Svotwa94Zvacho17162" ) {
        try {
            byte[] clearBytes = Encoding.Unicode.GetBytes( clearText );
            using ( Aes encryptor = Aes.Create() ) {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes( _sEncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 } );
                encryptor.Key = pdb.GetBytes( 32 );
                encryptor.IV = pdb.GetBytes( 16 );
                using ( MemoryStream ms = new MemoryStream() ) {
                    using ( CryptoStream cs = new CryptoStream( ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write ) ) {
                        cs.Write( clearBytes, 0, clearBytes.Length );
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String( ms.ToArray() );
                }
            }
            return clearText;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "UserClass", "encryptPwd", _Ex.Message );
            return "";
        }
    }

    public string decryptPwd( string cipherText, string _sEncryptionKey = "Zvibaba88Svotwa94Zvacho17162" ) {
        try {
            byte[] cipherBytes = Convert.FromBase64String( cipherText );
            using ( Aes encryptor = Aes.Create() ) {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes( _sEncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 } );
                encryptor.Key = pdb.GetBytes( 32 );
                encryptor.IV = pdb.GetBytes( 16 );
                using ( MemoryStream ms = new MemoryStream() ) {
                    using ( CryptoStream cs = new CryptoStream( ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write ) ) {
                        cs.Write( cipherBytes, 0, cipherBytes.Length );
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString( ms.ToArray() );
                }
            }
            return cipherText;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "UserClass", "decryptPwd", _Ex.Message );
            return "";
        }
    }

    public static bool exists( string _sGuid ) {
        try {
            string _sQuery = @"Select * 
                                    from tbl_user
                                    where uuid= '" + _sGuid + "'";
            return SqlClass.numRow( _sQuery ) > 0;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "UserClass", "exists", _Ex.Message );
        }
        return false;
    }

    public static bool emailTaken( string _sEmail, string _sUserUuid = "" ) {
        try {
            string _sQuery = @"Select * 
                                    from tbl_user
                                    where email ='" + _sEmail.Trim() + @"'"
                                   + ( _sUserUuid.Trim() == string.Empty ? ";" : " and uuid <> '" + _sUserUuid + "';" );
            return SqlClass.numRow( _sQuery ) > 0;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "UserClass", "emailTaken", _Ex.Message );
        }
        return false;
    }

    public static bool UsernameNotInUse( string _sUsername, string _sUserUuid = "", bool _bCheckPilot = false ) {
        try {
            string _sQuery = @"Select * 
                                    from tbl_user
                                    where username ='" + _sUsername.Trim() + @"'"
                                   + ( _sUserUuid.Trim() == string.Empty ? ";" : " and " + ( _bCheckPilot ? "pilot_uuid" : "uuid" ) + " <> '" + _sUserUuid + "';" );
            return SqlClass.numRow( _sQuery ) == 0;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "UserClass", "UsernameNotInUse", _Ex.Message );
        }
        return false;
    }
    #endregion
}