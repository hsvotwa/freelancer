﻿using System;
using System.Collections.Generic;
using System.Data;

public class Review {
    #region Member Variables
    protected string _sUuid;
    protected bool _bOnTime;
    protected bool _bOnBudget;
    protected int _iRating;
    protected string _sDescription;
    protected string _sLinkUuid;
    protected string _sUserUuid;
    #endregion

    #region Constructors
    public Review() {
    }

    public Review( string _sUuid, bool _bOnTime, bool _bOnBudget, int _iRating, string _sDescription, string _sLinkUuid, string _sUserUuid ) {
        this._sUuid = _sUuid;
        this._bOnTime = _bOnTime;
        this._bOnBudget = _bOnBudget;
        this._iRating = _iRating;
        this._sDescription = _sDescription;
        this._sLinkUuid = _sLinkUuid;
        this._sUserUuid = _sUserUuid;
    }
    #endregion

    #region Public Properties
    public string Uuid {
        get { return _sUuid; }
        set { _sUuid = value; }
    }
    public bool OnTime {
        get { return _bOnTime; }
        set { _bOnTime = value; }
    }
    public bool OnBudget {
        get { return _bOnBudget; }
        set { _bOnBudget = value; }
    }
    public int Rating {
        get { return _iRating; }
        set { _iRating = value; }
    }
    public string Description {
        get { return _sDescription; }
        set { _sDescription = value; }
    }
    public string LinkUuid {
        get { return _sLinkUuid; }
        set { _sLinkUuid = value; }
    }
    public string UserUuid {
        get { return _sUserUuid; }
        set { _sUserUuid = value; }
    }
    public static string g_sTableName = SqlTable.tbl_review.ToString();
    #endregion

    #region Public Properties
    public bool set() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += ( _bExist ? "update " : "insert into " ) + g_sTableName;
            _sQuery += " set on_time = '" + this.OnTime + "',";
            _sQuery += " on_budget = '" + this.OnBudget + "',";
            _sQuery += " rating = '" + this.Rating + "',";
            _sQuery += " description = '" + Conv.clean( this.Description ) + "',";
            _sQuery += " link_uuid = '" + Conv.clean( this.LinkUuid ) + "',";
            _sQuery += " user_uuid = '" + Conv.clean( this.UserUuid ) + "',";
            _sQuery += ( !_bExist ? ", created = now()" : "" );
            _sQuery += ( !_bExist ? ", uuid = " : " where uuid = " ) + "'" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Review", "set", _Ex.Message );
        }
        return false;
    }

    public bool delete() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += "update " + g_sTableName;
            _sQuery += " set soft_del = now() where uuid = '" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Review", "delete", _Ex.Message );
        }
        return false;
    }

    public bool exist( string _sUuid ) {
        try {
            return SqlClass.numRow( "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'" ) > 0;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Review", "exist", _Ex.Message );
        }
        return false;
    }

    public static Review getObj( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            DataRow _Dr = SqlClass.getDataRow( _sQuery );
            if ( _Dr == null ) {
                return null;
            }
            return new Review(
                _Dr["uuid"].ToString(),
                 int.Parse( _Dr["on_time"].ToString() ) == ( int )YesNo.yes,
                int.Parse( _Dr["on_budget"].ToString() ) == ( int )YesNo.yes,
                 int.Parse( _Dr["rating"].ToString() ),
                _Dr["description"].ToString(),
                _Dr["link_uuid"].ToString(),
                _Dr["user_uuid"].ToString()
                );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Review", "get", _Ex.Message );
        }
        return null;
    }

    public static dynamic getRow( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            return SqlClass.getDataRow( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Review", "get", _Ex.Message );
        }
        return null;
    }

    public static string deterQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
        try {
            _sSearchText = Conv.clean( _sSearchText.Trim() );
            string _sQuery = @"select 
                                            u.uuid,
                                            u.username,
                                            u.first_name,
                                            u.last_name,
                                            u.email,
                                            u.cell_no,
                                            ifnull(concat(p.last_name, ' ', p.first_name),'') as pilot,
                                            s.name as status
                                        from tbl_user u
                                        inner join lu_u_status s on s.id = u.enum_status_id
                                        left outer join tbl_p_pilot p on p.uuid= u.pilot_uuid
                                        where 1=1";
            _sQuery += ( _sUuid == "" || _sUuid == "0" ? "" : " and ac.uuid = '" + _sUuid + "'" );
            _sQuery += ( _sStatus.Trim() == "" || _sStatus == "0" ? "" : " and u.enum_status_id = '" + _sStatus + "'" );
            _sQuery += ( _sIsPilot.Trim() == "" || _sIsPilot == "0" ? "" : " and u.is_pilot = '" + _sIsPilot + "'" );
            _sQuery += ( _sSearchText == "" ? "" : " and (u.username like '%" + _sSearchText + "%' or  u.first_name like '%" + _sSearchText + @"%' or u.last_name like '%" + _sSearchText + @"%'
                                                         or u.email like '%" + _sSearchText + "%' or p.first_name like '" + _sSearchText + @"'
                                                          or p.last_name like '%" + _sSearchText + "%')" );
            _sQuery += " order by s.name, u.last_name, u.first_name;";
            return _sQuery;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Review", "deterQuery", _Ex.Message );
        }
        return String.Empty;
    }

    //public static IEnumerable<dynamic> getFromQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
    //    try {
    //        return SqlClass.getIEnumerableList( deterQuery( _sUuid, _sStatus, _sIsPilot, _sSearchText ) );
    //    } catch ( Exception _Ex ) {
    //        ErrorHandler.handleError( "Class", "Review", "deterQuery", _Ex.Message );
    //    }
    //    return null;
    //}
    #endregion
}