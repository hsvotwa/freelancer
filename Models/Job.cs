﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

public class Job {
    #region Member Variables
    protected string _sUuid;
    protected string _sTitle;
    protected JobMode _JobMode;
    protected int _iJobCategoryId;
    protected string _sLocation;
    protected decimal _dMinSalary;
    protected decimal _dMaxSalary;
    protected string _sDescription;
    protected string _sUserUuid;
    protected DateTime _dtExpiryDate;
    #endregion
    #region Constructors
    public Job() { }
    public Job( string _sUuid, string _sTitle, JobMode _JobMode, int _iJobCategoryId, string _sLocation,
        decimal _dMinSalary, decimal _dMaxSalary, string _sDescription, string _sUserUuid, DateTime _dtExpiryDate ) {
        this._sUuid = _sUuid;
        this._sTitle = _sTitle;
        this._JobMode = _JobMode;
        this._iJobCategoryId = _iJobCategoryId;
        this._sLocation = _sLocation;
        this._dMinSalary = _dMinSalary;
        this._dMaxSalary = _dMaxSalary;
        this._sDescription = _sDescription;
        this._sUserUuid = _sUserUuid;
        this._dtExpiryDate = _dtExpiryDate;
    }
    #endregion
    #region Public Properties
    public string Uuid {
        get { return _sUuid; }
        set { _sUuid = value; }
    }
    public string Title {
        get { return _sTitle; }
        set { _sTitle = value; }
    }
    public JobMode JobMode {
        get { return _JobMode; }
        set { _JobMode = value; }
    }
    public int JobCategory {
        get { return _iJobCategoryId; }
        set { _iJobCategoryId = value; }
    }
    public string Location {
        get { return _sLocation; }
        set { _sLocation = value; }
    }
    public decimal MinSalary {
        get { return _dMinSalary; }
        set { _dMinSalary = value; }
    }
    public decimal MaxSalary {
        get { return _dMaxSalary; }
        set { _dMaxSalary = value; }
    }
    public string Description {
        get { return _sDescription; }
        set { _sDescription = value; }
    }
    public string UserUuid {
        get { return _sUserUuid; }
        set { _sUserUuid = value; }
    }
    public DateTime ExpiryDate {
        get { return _dtExpiryDate; }
        set { _dtExpiryDate = value; }
    }
    public static string g_sTableName = SqlTable.tbl_job.ToString();
    #endregion

    #region Public Properties
    public bool set() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += ( _bExist ? "update " : "insert into " ) + g_sTableName;
            _sQuery += " set title = '" + Conv.clean( this.Title ) + "',";
            _sQuery += " enum_job_mode_id = '" + (int)this.JobMode + "',";
            _sQuery += " enum_category_id = '" + this.JobCategory + "',";
            _sQuery += " location = '" + Conv.clean( this.Location ) + "',";
            _sQuery += " min_salary = '" + this.MinSalary + "',";
            _sQuery += " max_salary = '" + this.MaxSalary + "',";
            _sQuery += " description = '" + Conv.clean( this.Description ) + "',"; ;
            _sQuery += " user_uuid = '" + Conv.clean( this.UserUuid ) + "',";
            _sQuery += " expiry_date = '" + this.ExpiryDate.ToString( Constant.SQL_DATE_FORMAT_LONG ) + "',";
            _sQuery += " last_modified = now()";
            _sQuery += ( !_bExist ? ", created = now()" : "" );
            _sQuery += ( !_bExist ? ", uuid = " : " where uuid = " ) + "'" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Job", "set", _Ex.Message );
        }
        return false;
    }

    public bool delete() {
        try {
            bool _bExist = this.exist( this.Uuid );
            string _sQuery = String.Empty;
            _sQuery += "update " + g_sTableName;
            _sQuery += " set soft_del = now() where uuid = '" + this.Uuid + "'";
            return SqlClass.execCmdText( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Job", "unset", _Ex.Message );
        }
        return false;
    }

    public bool exist( string _sUuid ) {
        try {
            return SqlClass.numRow( "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'" ) > 0;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Job", "exist", _Ex.Message );
        }
        return false;
    }

    public static Job getObj( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            DataRow _Dr = SqlClass.getDataRow( _sQuery );
            if ( _Dr == null ) {
                return null;
            }
            return new Job(
                _Dr["uuid"].ToString(),
                _Dr["title"].ToString(),
                ( JobMode )int.Parse( _Dr["enum_job_mode_id"].ToString() ),
               int.Parse( _Dr["enum_category_id"].ToString() ),
                _Dr["location"].ToString(),
                decimal.Parse( _Dr["min_salary"].ToString() ),
                decimal.Parse( _Dr["max_salary"].ToString() ),
                _Dr["description"].ToString(),
                _Dr["user_uuid"].ToString(),
                DateTime.Parse( _Dr["expiry_date"].ToString() )
                );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Job", "getObj", _Ex.Message );
        }
        return null;
    }

    public static dynamic getRow( string _sUuid ) {
        try {
            string _sQuery = "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'";
            return SqlClass.getDataRow( _sQuery );
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Job", "get", _Ex.Message );
        }
        return null;
    }

    public static string deterQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
        try {
            _sSearchText = Conv.clean( _sSearchText.Trim() );
            string _sQuery = @"select 
                                            u.uuid,
                                            u.username,
                                            u.first_name,
                                            u.last_name,
                                            u.email,
                                            u.cell_no,
                                            ifnull(concat(p.last_name, ' ', p.first_name),'') as pilot,
                                            s.name as status
                                        from tbl_user u
                                        inner join lu_u_status s on s.id = u.enum_status_id
                                        left outer join tbl_p_pilot p on p.uuid= u.pilot_uuid
                                        where 1=1";
            _sQuery += ( _sUuid == "" || _sUuid == "0" ? "" : " and ac.uuid = '" + _sUuid + "'" );
            _sQuery += ( _sStatus.Trim() == "" || _sStatus == "0" ? "" : " and u.enum_status_id = '" + _sStatus + "'" );
            _sQuery += ( _sIsPilot.Trim() == "" || _sIsPilot == "0" ? "" : " and u.is_pilot = '" + _sIsPilot + "'" );
            _sQuery += ( _sSearchText == "" ? "" : " and (u.username like '%" + _sSearchText + "%' or  u.first_name like '%" + _sSearchText + @"%' or u.last_name like '%" + _sSearchText + @"%'
                                                         or u.email like '%" + _sSearchText + "%' or p.first_name like '" + _sSearchText + @"'
                                                          or p.last_name like '%" + _sSearchText + "%')" );
            _sQuery += " order by s.name, u.last_name, u.first_name;";
            return _sQuery;
        } catch ( Exception _Ex ) {
            ErrorHandler.handleError( "Class", "Job", "deterQuery", _Ex.Message );
        }
        return String.Empty;
    }

    //public static IEnumerable<dynamic> getFromQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
    //    try {
    //        return SqlClass.getIEnumerableList( deterQuery( _sUuid, _sStatus, _sIsPilot, _sSearchText ) );
    //    } catch ( Exception _Ex ) {
    //        ErrorHandler.handleError( "Class", "Job", "deterQuery", _Ex.Message );
    //    }
    //    return null;
    //}
    #endregion
}