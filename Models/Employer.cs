﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Freelancer.Models {
    public class Employer {
        #region Member Variables
        protected string _sUuid;
        protected string _sCountryId;
        protected string _sName;
        protected string _sDetail;
        protected Status _Status;
        protected bool _bVerified;
        protected string _sLogoFileName;
        protected string _sLocation;
        protected string _sTwitterLink;
        protected string _sFacebookLink;
        protected string _sLinkedinLink;
        private string _sWebsiteLink;
        private string _sTagline;
        private string _sBusinessTags;
        #endregion

        #region Constructors
        public Employer() {
        }

        public Employer( string _sUuid, string _sCountryId, string _sName, string _sDetail, Status _Status,
                                string _sLogoFileName, string _sLocation, string _sTwitterLink, string _sFacebookLink, string _sLinkedinLink, string _sWebsiteLink, string _sTagline, string _sBusinessTags, bool _bVerified = false ) {
            this._sUuid = _sUuid;
            this._sCountryId = _sCountryId;
            this._sName = _sName;
            this._sDetail = _sDetail;
            this._Status = _Status;
            this._bVerified = _bVerified;
            this._sLogoFileName = _sLogoFileName;
            this._sLocation = _sLocation;
            this._sTwitterLink = _sTwitterLink;
            this._sFacebookLink = _sFacebookLink;
            this._sLinkedinLink = _sLinkedinLink;
            this._sWebsiteLink = _sWebsiteLink;
            this._sTagline = _sTagline;
            this._sBusinessTags = _sBusinessTags;
        }
        #endregion
        #region Public Properties
        public string Uuid {
            get { return _sUuid; }
            set { _sUuid = value; }
        }
        public string CountryId {
            get { return _sCountryId; }
            set { _sCountryId = value; }
        }
        public string Name {
            get { return _sName; }
            set { _sName = value; }
        }
        public string Detail {
            get { return _sDetail; }
            set { _sDetail = value; }
        }
        public Status Status {
            get { return _Status; }
            set { _Status = value; }
        }
        public bool Verified {
            get { return _bVerified; }
            set { _bVerified = value; }
        }
        public string LogoFileName {
            get { return _sLogoFileName; }
            set { _sLogoFileName = value; }
        }
        public string Location {
            get { return _sLocation; }
            set { _sLocation = value; }
        }
        public string TwitterLink {
            get { return _sTwitterLink; }
            set { _sTwitterLink = value; }
        }
        public string FacebookLink {
            get { return _sFacebookLink; }
            set { _sFacebookLink = value; }
        }
        public string LinkedinLink {
            get { return _sLinkedinLink; }
            set { _sLinkedinLink = value; }
        }
        public string WebsiteLink {
            get { return _sWebsiteLink; }
            set { _sWebsiteLink = value; }
        }
        public string TagLine {
            get { return _sTagline; }
            set { _sTagline = value; }
        }
        public string BusinessTags {
            get { return _sBusinessTags; }
            set { _sBusinessTags = value; }
        }

        public static string g_sTableName = SqlTable.tbl_employer.ToString();
        #endregion

        #region Public Properties
        public bool set() {
            try {
                bool _bExist = this.exist( this.Uuid );
                string _sQuery = String.Empty;
                _sQuery += ( _bExist ? "update " : "insert into " ) + g_sTableName;
                _sQuery += " set enum_country_id = '" + Conv.clean( this.CountryId ) + "',";
                _sQuery += "enum_status_id = '" + (int)this.Status + "',";
                _sQuery += "name = '" + Conv.clean( this.Name ) + "',";
                _sQuery += "detail = '" + Conv.clean( this.Detail ) + "',";
                _sQuery += "logo_file_name = '" + Conv.clean( this.LogoFileName ) + "',";
                _sQuery += "location = '" + Conv.clean( this.Location ) + "',";
                _sQuery += "twitter_link = '" + Conv.clean( this.TwitterLink ) + "',";
                _sQuery += "facebook_link = '" + Conv.clean( this.FacebookLink ) + "',";
                _sQuery += "linkedin_link = '" + Conv.clean( this.LinkedinLink ) + "',";
                _sQuery += "website_link = '" + Conv.clean( this.WebsiteLink ) + "',";
                _sQuery += "business_tags = '" + Conv.clean( this.BusinessTags ) + "',";
                _sQuery += "tag_line = '" + Conv.clean( this.TagLine ) + "',";
                _sQuery += "last_modified = now()";
                _sQuery += ( !_bExist ? ", created = now()" : "" );
                _sQuery += ( !_bExist ? ", uuid = " : " where uuid = " ) + "'" + this.Uuid + "'";
                return SqlClass.execCmdText( _sQuery );
            } catch ( Exception _Ex ) {
                ErrorHandler.handleError( "Class", "Employer", "set", _Ex.Message );
            }
            return false;
        }

        public bool unset() {
            try {
                bool _bExist = this.exist( this.Uuid );
                string _sQuery = String.Empty;
                _sQuery += "update " + g_sTableName;
                _sQuery += " set soft_del = now() where uuid = '" + this.Uuid + "'";
                return SqlClass.execCmdText( _sQuery );
            } catch ( Exception _Ex ) {
                ErrorHandler.handleError( "Class", "Employer", "unset", _Ex.Message );
            }
            return false;
        }

        public bool exist( string _sUuid ) {
            try {
                return SqlClass.numRow( "select * from " + g_sTableName + " where uuid = '" + _sUuid + "'" ) > 0;
            } catch ( Exception _Ex ) {
                ErrorHandler.handleError( "Class", "Employer", "exist", _Ex.Message );
            }
            return false;
        }

        public static Employer getObj( string _sUuid ) {
            try {
                string _sQuery = "select *, if(verified is null, 2, 1) _verified  from " + g_sTableName + " where uuid = '" + _sUuid + "'";
                DataRow _Dr = SqlClass.getDataRow( _sQuery );
                if ( _Dr == null ) {
                    return null;
                }
                return new Employer(
                        _Dr["uuid"].ToString(),
                        _Dr["enum_country_id"].ToString(),
                        _Dr["name"].ToString(),
                        _Dr["detail"].ToString(),
                        (Status)int.Parse( _Dr["enum_status_id"].ToString() ),
                        _Dr["logo_file_name"].ToString(),
                        _Dr["location"].ToString(),
                        _Dr["twitter_link"].ToString(),
                        _Dr["facebook_link"].ToString(),
                        _Dr["linkedin_link"].ToString(),
                        _Dr["website_link"].ToString(),
                        _Dr["tag_line"].ToString(),
                        _Dr["business_tags"].ToString(),
                        int.Parse( _Dr["_verified"].ToString() ) == (int)YesNo.yes
                        );
            } catch ( Exception _Ex ) {
                ErrorHandler.handleError( "Class", "Employer", "get", _Ex.Message );
            }
            return null;
        }

        public static string getLogo( string _sUuid ) {
            string _sPath = "/images/company-logo-placeholder.png";
            try {
                string _sQuery = "select server_name from tbl_file where link_uuid = '" + _sUuid + "' and enum_type_id= '" + (int)FileType.company_logo + "'  order by created desc limit 1";//Change
                DataRow _Dr = SqlClass.getDataRow( _sQuery );
                if ( _Dr != null ) {
                    _sPath = "/" + Constant.UPLOADS_FOLDER + "/" + _Dr["server_name"].ToString();
                }
            } catch ( Exception _Ex ) {
                ErrorHandler.handleError( "Class", "File", "get", _Ex.Message );
            }
            return _sPath;
        }

        public DataRow getDataRow() {
            try {
                string _sQuery = @"select 
                                                e.*, 
                                                e.name as company_name,
                                                u.uuid as user_uuid,
                                                u.company_uuid,
                                                u.name as user_name,
                                                u.surname,
                                                u.email,
                                                u.enum_account_type_id,
                                                u.2_step_verification,
                                                u.enum_status_id
                                            from tbl_user u
                                            left join tbl_employer e on e.uuid = u.company_uuid
                                            where e.uuid = '" + this.Uuid + "'";
                return SqlClass.getDataRow( _sQuery );
            } catch ( Exception _Ex ) {
                ErrorHandler.handleError( "Class", "Employer", "get", _Ex.Message );
            }
            return null;
        }

        public static string deterQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
            try {
                _sSearchText = Conv.clean( _sSearchText.Trim() );
                string _sQuery = @"select 
                                            u.uuid,
                                            u.username,
                                            u.first_name,
                                            u.last_name,
                                            u.email,
                                            u.cell_no,
                                            ifnull(concat(p.last_name, ' ', p.first_name),'') as pilot,
                                            s.name as status
                                        from tbl_user u
                                        inner join lu_u_status s on s.id = u.enum_status_id
                                        left outer join tbl_p_pilot p on p.uuid= u.pilot_uuid
                                        where 1=1";
                _sQuery += ( _sUuid == "" || _sUuid == "0" ? "" : " and ac.uuid = '" + _sUuid + "'" );
                _sQuery += ( _sStatus.Trim() == "" || _sStatus == "0" ? "" : " and u.enum_status_id = '" + _sStatus + "'" );
                _sQuery += ( _sIsPilot.Trim() == "" || _sIsPilot == "0" ? "" : " and u.is_pilot = '" + _sIsPilot + "'" );
                _sQuery += ( _sSearchText == "" ? "" : " and (u.username like '%" + _sSearchText + "%' or  u.first_name like '%" + _sSearchText + @"%' or u.last_name like '%" + _sSearchText + @"%'
                                                         or u.email like '%" + _sSearchText + "%' or p.first_name like '" + _sSearchText + @"'
                                                          or p.last_name like '%" + _sSearchText + "%')" );
                _sQuery += " order by s.name, u.last_name, u.first_name;";
                return _sQuery;
            } catch ( Exception _Ex ) {
                ErrorHandler.handleError( "Class", "Employer", "deterQuery", _Ex.Message );
            }
            return String.Empty;
        }

        //public static IEnumerable<dynamic> getFromQuery( string _sUuid = "", string _sStatus = "", string _sIsPilot = "", string _sSearchText = "" ) {
        //    try {
        //        return SqlClass.getIEnumerableList( deterQuery( _sUuid, _sStatus, _sIsPilot, _sSearchText ) );
        //    } catch ( Exception _Ex ) {
        //        ErrorHandler.handleError( "Class", "Employer", "deterQuery", _Ex.Message );
        //    }
        //    return null;
        //}
        #endregion
    }
}